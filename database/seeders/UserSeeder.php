<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->times(10)->create();

        $user = User::where('email', 'angwamoses@yahoo.com')->first();

        if ((!$user) || $user->is_admin === 0) {
            User::create([
                'first_name' => 'Angwa',
                'last_name' => 'Moses',
                'role' => 'admin',
                'email' => 'angwamoses@yahoo.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$//R2s4a3mgOt8v5ZW4VT2.PMQ4Ae8iHvNsCoss2.K87gS7vnQXEgq', // 12345678
                'address' => 'Lagos, Nigeria',
                'phone' => '+23472322222',
                'state' => 'benue',
            ]);
        }
    }
}
