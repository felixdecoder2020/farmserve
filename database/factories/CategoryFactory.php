<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $bg = $this->faker->randomElement(['products/seeders/center-img.jpg', 'products/seeders/left-img.jpg','right-img.jpg']);

        return [
            'uuid' => Str::orderedUuid(),
            'name' => fake()->name(),
            'background' => $bg,
            'featured' => $this->faker->randomElement([true, false])
        ];
    }
}
