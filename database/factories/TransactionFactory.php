<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'uuid' => Str::orderedUuid(),
            'amount' => $this->faker->randomFloat(2, 0, 10000),
            'user_id' => User::all()->random()->id,
            'payment_method' => "card",
            'status' => $this->faker->randomElement([true, false]),
            'delivery' => $this->faker->randomElement(['pending', 'processed','delivered'])

        ];
    }
}
