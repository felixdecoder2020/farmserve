<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'transaction_id' => Transaction::all()->random()->id,
            'product_id' => Product::all()->random()->id,
            'quantity' => $this->faker->randomNumber(3, 10),
            'price' => $this->faker->randomFloat(2, 0, 10000),
        ];
    }
}
