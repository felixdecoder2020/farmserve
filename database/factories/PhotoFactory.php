<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Photo>
 */
class PhotoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $image = $this->faker->randomElement([
            'products/seeders/product2.jpg',
            'products/seeders/product3.jpg',
            'products/seeders/product4.jpg',
            'products/seeders/product1.jpg',
            'products/seeders/product-img-1.jpg',
            'products/seeders/product-img-2.jpg',
            'products/seeders/product-img-3.jpg',
            'products/seeders/product-img-4.jpg',
            'products/seeders/product-img-5.jpg',
            'products/seeders/product-img-6.jpg',
            'products/seeders/product-img-7.jpg',
            'products/seeders/product-img-8.jpg',
            'products/seeders/product-img-9.jpg',
            'products/seeders/product-img-10.jpg',
        ]);

        return [
            'uuid' => Str::orderedUuid(),
            'product_id' => Product::all()->random()->id,
            'image' => $image
        ];
    }
}
