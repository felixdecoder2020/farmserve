<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $type = $this->faker->randomElement(['featured', 'deal']);

        return [
            'uuid' => Str::orderedUuid(),
            'name' => fake()->name(),
            'user_id' => User::all()->random()->id,
            'category_id' => Category::all()->random()->id,
            'type' => $type,
            'price' => $this->faker->randomFloat(2, 0, 10000),
            'quantity' => $this->faker->randomNumber(3, 1000),
            'description' => fake()->sentence(),
        ];
    }
}
