<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cart>
 */
class CartFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'uuid' => Str::orderedUuid(),
            'product_id' => Product::all()->random()->id,
            'user_id' => User::all()->random()->id,
            'quantity' => $this->faker->randomNumber(3, 10),
        ];
    }
}
