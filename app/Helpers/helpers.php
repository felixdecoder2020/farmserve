<?php

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

function vendorList($type)
{
    switch ($type) {
        case 'latest':
            return User::orderBy('id', 'desc')->limit(5)->get();
            break;
        case 'popular':
            return User::limit(5)->get();
            break;
        default:
            return User::inRandomOrder()->limit(4)->get();
    }
}

function randomCategory()
{
    return Category::whereNotNull('background')->inRandomOrder()->limit(3)->get(['name', 'id', 'background']);
}

function findProductsForCategory($id, $limit = 3)
{
    return Product::where('category_id', $id)->limit($limit)->get();
}

function categories()
{
    return Category::inRandomOrder()->limit(11)->get();
}

function latestProducts()
{
    return Product::orderBy('id', 'desc')->limit(12)->get();
}
