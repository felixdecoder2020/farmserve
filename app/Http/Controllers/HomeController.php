<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function index()
    {
        $title = "Home - Farm products";

        $subscribe = true;

        if (Cache::has('subscribe')) {
            $subscribe = false;
        }


        return view('index', compact('title', 'subscribe'));
    }

    public function dashboard()
    {
        $title = "dashboard";
        $data = Transaction::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->limit(30)->get();

        $order = [
            "pending" => $this->orderStatus(0),
            "successful" => $this->orderStatus(1),
            "delivered"  => $this->deliveryStatus('delivered'),
            "pending_delivery"  => $this->deliveryStatus('pending'),
            "processed"  => $this->deliveryStatus('processed'),
        ];

        return view('dashboard', compact('title', 'data', 'order'));
    }

    private function orderStatus($status)
    {
        return Transaction::where('user_id', Auth::user()->id)->where('status', $status)->count();
    }

    private function deliveryStatus($status)
    {
        return Transaction::where('user_id', Auth::user()->id)->where('delivery', $status)->count();
    }
}
