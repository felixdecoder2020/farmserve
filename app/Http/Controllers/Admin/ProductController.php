<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Str;

class ProductController extends Controller
{
    public function index()
    {
        $title = "products";
        $data =  Product::orderBy('id', 'desc')->get();

        return view('admin.product.index', compact('title', 'data'));
    }

    public function create()
    {
        $title = "products";
        $data = Category::orderBy('id', 'desc')->get(['id', 'name']);

        return view('admin.product.create', compact('title', 'data'));
    }

    public function add(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:40',
            'category' => 'required|string|exists:categories,id',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'description' => 'required|string|max:255',
            'photo' => 'required|array'
        ]);

        $product = Product::create([
            'uuid' => Str::orderedUuid(),
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'category_id' => $request->category,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
        ]);

        for ($i = 0; $i <= count($request->photo); $i++) {

            $file = $request->file('photo')[$i] ?? null;

            if ($file == null) {
                break;
            }

            if (!in_array($file->getClientOriginalExtension(), ['jpeg', 'jpg', 'png', 'gif'])) {
                continue;
            }

            $image = $file->store('products', 'public_uploads');

            Photo::create([
                'uuid' => Str::orderedUuid(),
                'product_id' => $product->id,
                'image' => $image
            ]);
        }

        return back()->with('success', 'Product uploaded successfully');
    }

    public function edit(Product $product)
    {
        $title = "update product";
        $data = $product;
        $category = Category::orderBy('id', 'desc')->get(['id', 'name']);

        return view('admin.product.update', compact('title', 'data', 'category'));
    }

    public function update(Product $product, Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:40',
            'category' => 'required|string|exists:categories,id',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'description' => 'required|string|max:255',
            'photo' => 'nullable|array'
        ]);

        $product->update([
            'name' => $request->name,
            'category_id' => $request->category,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'type' => $request->type  !== 'none' ? $request->type : null,
            'description' => $request->description,
        ]);

        if (!empty($request->photo)) {
            for ($i = 0; $i <= count($request->photo); $i++) {
                $file = $request->file('photo')[$i] ?? null;

                if ($file == null) {
                    break;
                }

                if (!in_array($file->getClientOriginalExtension(), ['jpeg', 'jpg', 'png', 'gif'])) {
                    continue;
                }

                $image = $file->store('products', 'public_uploads');

                Photo::create([
                    'uuid' => Str::orderedUuid(),
                    'product_id' => $product->id,
                    'image' => $image,
                ]);
            }
        }

        return back()->with('success', 'Product updated successfully.');
    }

    public function deletePhoto(Photo $photo)
    {
        $photo->delete();

        return back()->with('success', 'Photo Removed successfully');
    }
}
