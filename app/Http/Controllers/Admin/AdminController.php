<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class AdminController extends Controller
{
    public function index()
    {
        $title = "Admin";
        $user = Auth::user();

        $total_customers = User::where('role', 'customer')->count();
        $total_retailers = User::where('role', 'retailer')->count();
        $total_sales = Transaction::where('status', true)->count();
        $total_products = Product::count();
        $withdrawals = Withdrawal::where('status', 'successful')->count();

        if ($user->role != 'admin') {
            $total_sales = Transaction::where('user_id', $user->id)->where('status', true)->count();
            $total_products = Product::where('user_id', $user->id)->count();
            $withdrawals = Withdrawal::where('user_id', $user->id)->where('status', 'successful')->count();
        }

        $days_data = json_encode($this->calculateRatePerDay());
        $days = json_encode($this->retrieveDays());

        $data = [
            "total_customers" => $total_customers,
            "total_retailers" => $total_retailers,
            "total_sales" => $total_sales,
            "total_products" => $total_products,
            "withdrawals" => $withdrawals,
        ];

        return view('admin', compact('title', 'data', 'days_data', 'days'));
    }

    public function password()
    {
        $title = "update password";

        return view('admin.profile.password', compact('title'));
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = Auth::user();

        if (!Hash::check($request->old_password, $user->password)) {
            return back()->with('error', 'old password is invalid');
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return back()->with('success', 'password updated successfully');
    }

    public function profile()
    {
        $title = "update profile";

        return view('admin.profile.profile', compact('title'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:15'],
            'state' => ['required', 'string', 'max:30'],
            'address' => ['required', 'string', 'max:255'],
        ]);

        Auth::user()->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'state' => $request->state,
            'address' => $request->address,
        ]);

        return back()->with('success', 'Profile Updated successfully.');
    }

    public function withdraw()
    {
        $title = "Withdrawal requests";

        $withdrawals = Withdrawal::where('status', 'pending')->orderBy('id', 'desc')->get();

        return view('admin.withdrawal.approve', compact('title', 'withdrawals'));
    }

    public function manageWithdrawal(Withdrawal $withdrawal, Request $request)
    {
        if (!empty($request->success)) {
            $withdrawal->status = "successful";
        }

        if (!empty($request->decline)) {
            $user = User::find($withdrawal->user_id);

            $user->increment('balance', $withdrawal->amount);
            $withdrawal->status = "declined";
        }

        $withdrawal->save();

        return back()->with('success', 'Withdrawal request marked successfully.');
    }

    private function calculateRatePerDay()
    {
        $now = Carbon::now()->subWeek(1);

        $monday = $now->startOfWeek()->format('Y-m-d H:i');
        $tuesday = $now->startOfWeek()->addDay(1)->format('Y-m-d H:i');
        $wednesday = $now->startOfWeek()->addDay(2)->format('Y-m-d H:i');
        $thursaday = $now->startOfWeek()->addDay(3)->format('Y-m-d H:i');
        $friday = $now->startOfWeek()->addDay(4)->format('Y-m-d H:i');
        $saturday = $now->startOfWeek()->addDay(5)->format('Y-m-d H:i');
        $sunday = $now->startOfWeek()->addDay(6)->format('Y-m-d H:i');

        // dd($monday, $tuesday, $wednesday, $thursaday, $friday, $saturday, $sunday);

        $data = [
            $this->getDataPerDay($monday),
            $this->getDataPerDay($tuesday),
            $this->getDataPerDay($wednesday),
            $this->getDataPerDay($thursaday),
            $this->getDataPerDay($friday),
            $this->getDataPerDay($saturday),
            $this->getDataPerDay($sunday),
        ];

        return $data;
    }

    private function getDataPerDay($day)
    {
        $transaction = Order::whereDate('created_at', $day);

        if (Auth::user()->role !== 'admin') {
            $transaction = DB::table('orders')
                ->join('products', 'orders.product_id', 'products.id')
                ->where('products.user_id', Auth::user()->id)
                ->whereDate('orders.created_at', $day);
        }

        return $transaction->count();
    }

    private function retrieveDays(): array
    {
        return [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ];
    }
}
