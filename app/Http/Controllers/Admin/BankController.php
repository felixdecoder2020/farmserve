<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\UserBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    public function index()
    {
        $title = "Bank";
        $data = Auth::user()->userBank;
        $banks = Bank::all();

        return view('admin.bank.index', compact('title', 'data','banks'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'bank' => 'required|max:100',
            'account_name' => 'required|max:200',
            'account_number' => 'required|max:10|min:10',
            'account_type' => 'required|max:100'
        ]);


        UserBank::updateOrCreate(['user_id'=>Auth::user()->id],[
            'bank_id' => $request->bank,
            'user_id' => Auth::user()->id,
            'account_number' => $request->account_number,
            'account_name' => $request->account_name,
            'account_type' => $request->account_type,
        ]);

        return back()->with('success','Bank added successfully.');
    }
}
