<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $title = "show orders";
        $data = Transaction::orderBy('id', 'desc')->get();

        return view('admin.order.index', compact('title', 'data'));
    }

    public function show(Transaction $order)
    {
        $title = "orders";
        $data = $order;

        return view('admin.order.show', compact('title', 'data'));
    }

    public function delivery(Transaction $order, Request $request)
    {
        $request->validate([
            'status' => 'required|string|max:40'
        ]);

        $order->update([
            'delivery' => $request->status
        ]);

        return back()->with('success', 'Product status updated successfully.');
    }
}
