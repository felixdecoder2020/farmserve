<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function merchant()
    {
        $title = "show merchants";
        $data = User::where('role', 'retailer')->orderBy('updated_at', 'desc')->get();

        return view('admin.merchant.index', compact('title', 'data'));
    }

    public function merchantSingle(User $user)
    {
        $title = "show merchant detail";
        $data = $user;

        return view('admin.merchant.show', compact('title', 'data'));
    }

    public function updateRole(User $user, Request $request)
    {
        $request->validate([
            'role' => 'required|string|max:40'
        ]);

        $user->update([
            'role' => $request->role
        ]);

        return back()->with('success', 'User role updated successfully');
    }

    public function users()
    {
        $title = "users";
        $data = User::where('role', '!=', 'retailer')->orderBy('updated_at', 'desc')->get();

        return view('admin.user.index', compact('title', 'data'));
    }

    public function singleUser(User $user)
    {
        $title = "show user detail";
        $data = $user;

        return view('admin.user.show', compact('title', 'data'));
    }
}
