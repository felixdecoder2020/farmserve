<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Str;

class CategoryController extends Controller
{
    public function index()
    {
        $title = "Categories";
        $data = Category::orderBy('id', 'desc')->get();

        return view('admin.category.index', compact('title', 'data'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:40'
        ]);

        Category::create([
            'uuid' => Str::orderedUuid(),
            'name' => $request->name
        ]);

        return back()->with('success', 'Category created successfully');
    }

    public function show(Category $category)
    {
        $title = "Update category";
        $data = $category;

        return view('admin.category.update', compact('title', 'data'));
    }

    public function update(Category $category, Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:40',
            'featured' => 'required|string|max:3',
            'background' => 'nullable|mimes:jpg,png,gif'
        ]);

        $image = null;

        if ($request->background) {
            $image = $request->file('background')->store('products', 'public_uploads');
        }

        $category->update([
            'name' => $request->name,
            'featured' => $request->featured,
            'background' => $image ?? $category->background
        ]);

        return back()->with('success', 'Category updated successfully.');
    }
}
