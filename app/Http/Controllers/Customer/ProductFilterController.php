<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductFilterController extends Controller
{
    public function category($category)
    {
        $title = "Filter by category";

        $category_id = Category::where('name', $category)->first();

        $product =  Product::where('category_id', $category_id->id)->paginate(10);

        return view('filter.category', compact('title', 'product'));
    }
    public function search(Request $request)
    {
        $title = "Filter by search";

        if ($request->category != 'all') {
            $category_id = Category::where('name', $request->category)->first();
            $data =  Product::like('name', $request->get('query'))->where('category_id', $category_id->id ?? 1);
        } else {
            $data =  Product::like('name', $request->get('query'));
        }

        $query = $request->get('query');
        $product = $data->get();

        return view('filter.search', compact('title', 'product', 'query'));
    }
}
