<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function show($transaction)
    {
        $title = "Show order details";

        $payment = Transaction::where('uuid', $transaction)->first();

        if (!$payment) {
            return back()->with('error', 'Transaction does not exist');
        }

        $data = $payment->orders;

        return view('order.show', compact('title', 'data', 'payment'));
    }

    public function withdrawal()
    {
        $title = "withdraw funds";

        $withdrawals =  Auth::user()->withdrawals;

        return view('admin.withdrawal.index', compact('title', 'withdrawals'));
    }

    public function withdraw(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric'
        ]);

        $user = Auth::user();

        if (!$user->userBank) {
            return back()->with('error', "Please add bank account before requesting for withdrawal");
        }

        if ($user->balance < $request->amount) {
            return back()->with('error', "Your account balance is less than the requested amount");
        }

        Withdrawal::create([
            'user_id' => $user->id,
            'account_number' => $user->userBank->account_number,
            'bank_name' => $user->userBank->bank->name,
            'account_name' => $user->userBank->account_name,
            'amount' => $request->amount,
        ]);

        $user->decrement('balance', $request->amount);

        return back()->with('success', 'Withdrawal request has been initiated successfully');
    }
}
