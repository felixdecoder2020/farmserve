<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show($product)
    {
        $title = "show product";

        $data = Product::where('uuid', $product)->firstOrFail();

        return view('single', compact('title', 'data'));
    }
}
