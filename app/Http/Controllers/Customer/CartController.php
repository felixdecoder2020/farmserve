<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Str;
use App\Services\PayStack;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function delete($cart)
    {
        $item = Cart::where('uuid', $cart)->first();

        if (!$item) {
            return back()->with('error', 'Unable to perform request. Please try again');
        }

        $item->delete();

        return back()->with('success', 'Item deleted from cart successfully.');
    }

    public function add($product)
    {
        $product = Product::where('uuid', $product)->first();

        if (!$product) {
            return back()->with('error', 'Unable to perform request. Please try again');
        }

        if ($product->quantity < 1) {
            return back()->with('error', 'Product out of stock');
        }

        $user = Auth::user();

        $cart = Cart::where('product_id', $product->id)->where('user_id', $user->id)->first();

        if ($cart) {
            return back()->with('error', 'This product has already been added to cart by you.');
        }

        Cart::create([
            'uuid' => Str::orderedUuid(),
            'product_id' => $product->id,
            'user_id' => $user->id,
            'quantity' => 1,
        ]);

        return back()->with('success', 'Item added to cart successfully.');
    }
    public function addMore($product, Request $request)
    {
        $product = Product::where('uuid', $product)->first();

        $request->validate([
            'quantity' => 'required|numeric'
        ]);

        if (!$product) {
            return back()->with('error', 'Unable to perform request. Please try again');
        }

        if ($product->quantity < $request->quantity) {
            return back()->with('error', 'Quantity inputed is more than products in stock');
        }

        $user = Auth::user();

        $cart = Cart::where('product_id', $product->id)->where('user_id', $user->id)->first();

        if ($cart) {
            return back()->with('error', 'This product has already been added to cart by you.');
        }

        Cart::create([
            'uuid' => Str::orderedUuid(),
            'product_id' => $product->id,
            'user_id' => $user->id,
            'quantity' => $request->quantity,
        ]);

        return back()->with('success', 'Item added to cart successfully.');
    }

    public function show()
    {
        $title = "my cart";
        $data = Auth::user()->carts;

        return view('cart', compact('title', 'data'));
    }

    public function update(Request $request)
    {
        if ($request->product != null) {

            foreach ($request->product as $key => $data) {

                Cart::where('uuid', $data)->update([
                    'quantity' => ($request->quantity[$key] < 0) ? 1 : $request->quantity[$key]
                ]);
            }

            return back()->with('success', 'Cart updated successfully. ');
        }

        return back()->with('error', 'Cannot update an empty cart!');
    }

    public function checkout()
    {
        $user = Auth::user();
        $sum = 0;

        foreach ($user->carts as $cart) {
            $sum += $cart->product->price * $cart->quantity;
        }

        $interest = (10 / 100) * $sum;
        $total = $interest + $sum;

        DB::beginTransaction();

        try {
            $transaction = Transaction::create([
                'uuid' => Str::orderedUuid(),
                'user_id' => $user->id,
                'amount' => $total,
                'payment_method' => 'card',
                'status' => false,
                'delivery' => 'pending',
            ]);

            if (!$transaction) {
                return back()->with('error', 'Unable to checkout cart. Please try again');
            }

            foreach ($user->carts as $cart) {
                Order::create([
                    'product_id' => $cart->product_id,
                    'transaction_id' => $transaction->id,
                    'quantity' => $cart->quantity,
                    'price' => $cart->product->price * $cart->quantity,
                ]);
            }

            $data = [
                "email" => Auth::user()->email,
                "amount" => (int) $transaction->amount + 1,
                "reference" => $transaction->uuid,
            ];

            $response = (new PayStack($data))->handlePayment();

            if (!$response["status"] === true) {
                Log::error('Error, something went wrong with paystack: ', $response);
                return back()->with('error', "Something went wrong. Please try again.");
            }

            $url = $response["data"]["authorization_url"];
            DB::commit();

            return redirect($url);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e);

            return back()->with('error', "Something went wrong. Please try again.");
        }
    }

    public function verify(Request $request)
    {
        $title = "verify payment";
        $user = Auth::user();

        $payment = Transaction::where('uuid', $request->reference)->first();

        if (!$payment) {
            return back()->with('error', "Payment Reference is not valid!");
        }

        $response = (new PayStack())->verifyPayment($request->reference);

        if ($response->status !== true) {
            return redirect()->route('index')->with('error', "Something went wrong!");
        }

        if ($response->data->status == 'success') {

            if ($payment->status != true) {
                $orders = Order::where('transaction_id', $payment->id)->get();

                foreach ($orders as $order) {
                    $product =  Product::find($order->product_id);
                    $owner = User::find($product->user_id);
                    $owner->increment('balance', $order->price);
                }

                $payment->update([
                    'status' => true
                ]);
            }

            Session::flash('success', 'Payment for order was successful!');
        } else {
            Session::flash('error', 'Your payment has not been approved. Please contact admin');
        }

        $user->carts->each(function ($cart) {
            $cart->delete();
        });

        $data =  $payment->orders;

        return view('order.verify', compact('title', 'data', 'payment'));
    }
}
