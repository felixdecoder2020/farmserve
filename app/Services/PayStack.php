<?php


namespace App\Services;

use Illuminate\Support\Facades\Http;
use Exception;
use Illuminate\Http\Client\RequestException;

class PayStack
{
    private $client;
    public $data;

    public function __construct(array $data = [])
    {
        $this->client = Http::baseUrl(config('paystack.base_url'))
            ->withHeaders(['Authorization' => 'Bearer ' . config('paystack.api_key')]);

        $this->data = $data;
    }

    public function handlePayment()
    {
        try {
            return $this->client->post(
                "/transaction/initialize",
                [
                    "email" => $this->data["email"],
                    "amount" => $this->data["amount"] * 100,
                    "reference" => $this->data["reference"],
                ]
            )->throw()->json();
        } catch (Exception $e) {
            if ($e instanceof RequestException && $e->response->status() === 404) {
                return false;
            }

            throw $e;
        }
    }

    public function verifyPayment($reference)
    {
        $transaction =  $this->client->get("/transaction/verify/{$reference}");

        return json_decode($transaction);
    }
}
