<?php

use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BankController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Customer\CartController;
use App\Http\Controllers\Customer\ProductController as CustomerProductController;
use App\Http\Controllers\Customer\ProductFilterController;
use App\Http\Controllers\Customer\ProfileControl;
use App\Http\Controllers\Customer\TransactionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');


Route::get('product/{product}', [CustomerProductController::class,  'show'])->name('customer.product.show');
Route::get('category/{category}', [ProductFilterController::class,  'category'])->name('filter.category');
Route::get('search', [ProductFilterController::class,  'search'])->name('filter.search');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');

    Route::get('cart/delete-item/{cart}', [CartController::class,  'delete'])->name('cart.delete.item');
    Route::get('cart/add-item/{product}', [CartController::class,  'add'])->name('cart.add.item');
    Route::post('cart/add-item/{product}', [CartController::class,  'addMore'])->name('post.add.cart');
    Route::get('cart/show', [CartController::class,  'show'])->name('cart.show');
    Route::post('cart/update', [CartController::class,  'update'])->name('cart.update');
    Route::post('cart/cehckout', [CartController::class,  'checkout'])->name('cart.checkout');
    Route::get('/payment/approve', [CartController::class, 'verify'])->name('paymount.approve');
    Route::get('/order/show/{transaction}', [TransactionController::class, 'show'])->name('customer.order.show');

    Route::post('/customer/update', [ProfileControl::class, 'update'])->name('customer.profile.update');
    Route::post('/customer/update-password', [ProfileControl::class, 'password'])->name('customer.password.update');

    Route::get('/admin/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

    Route::middleware(['isAdmin'])->group(function () {
        Route::get('admin/categories', [CategoryController::class, 'index'])->name('category');
        Route::post('admin/categories', [CategoryController::class, 'create'])->name('category.add');
        Route::get('admin/categories/{category}', [CategoryController::class, 'show'])->name('category.update');
        Route::post('admin/categories/{category}', [CategoryController::class, 'update'])->name('category.update.post');

        Route::get('admin/merchants', [UserController::class, 'merchant'])->name('admin.merchant');
        Route::get('admin/merchant/{user}', [UserController::class, 'merchantSingle'])->name('admin.merchant.single');
        Route::post('admin/role/update/{user}', [UserController::class, 'updateRole'])->name('admin.role.update');

        Route::get('admin/users', [UserController::class, 'users'])->name('admin.users');
        Route::get('admin/user/{user}', [UserController::class, 'singleUser'])->name('admin.user.single');
    });

    Route::get('admin/products', [ProductController::class, 'index'])->name('admin.products');
    Route::get('admin/product/create', [ProductController::class, 'create'])->name('admin.product.create');
    Route::post('admin/product/create', [ProductController::class, 'add'])->name('admin.product.create.submit');
    Route::get('admin/product/{product}', [ProductController::class, 'edit'])->name('admin.product.create.edit');
    Route::post('admin/product/{product}', [ProductController::class, 'update'])->name('admin.product.create.update');
    Route::post('admin/file/delete/{photo}', [ProductController::class, 'deletePhoto'])->name('remove.photo');

    Route::get('admin/orders', [OrderController::class, 'index'])->name('admin.orders');
    Route::get('admin/order/{order}', [OrderController::class, 'show'])->name('admin.order.find');
    Route::post('admin/order/delivery/{order}', [OrderController::class, 'delivery'])->name('update.delivery');


    Route::get('admin/password', [AdminController::class, 'password'])->name('admin.user.password');
    Route::post('admin/password', [AdminController::class, 'updatePassword'])->name('admin.submit.update_password');

    Route::get('admin/profile', [AdminController::class, 'profile'])->name('admin.user.profile');
    Route::post('admin/profile', [AdminController::class, 'updateProfile'])->name('admin.submit.profile_update');

    Route::get('admin/bank', [BankController::class, 'index'])->name('admin.bank.show');
    Route::post('admin/bank', [BankController::class, 'create'])->name('admin.bank.create');

    Route::get('admin/withdrawals', [TransactionController::class, 'withdrawal'])->name('admin.withdrawals');
    Route::post('admin/withdrawals', [TransactionController::class, 'withdraw'])->name('admin.withdrawals.post');
    Route::get('admin/withdrawals/requests', [AdminController::class, 'withdraw'])->name('admin.withdrawals.request');
    Route::post('admin/withdrawals/requests/{withdrawal}', [AdminController::class, 'manageWithdrawal'])->name('admin.withdrawals.manage');



    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
});

require __DIR__ . '/auth.php';
