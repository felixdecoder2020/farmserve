<div id="{{$address}}" class="popup-fcy">
    <div class="row">
        <div class="col-md-6 text-center"> <img src="{{  asset($modal->photos[0]->image) }}" alt="" title="" class="img-fluid"> </div>
        <div class="col-md-6">
            <div class="product_meta">
                <p>Availability :

                    @if($modal->quantity < 1) <span> Not in stock</span>
                        @else
                        <span> In stock</span>
                        @endif
                </p>
                <p>Categories : <span>{{$modal->category->name}}</span></p>
                <p>Tags : <span>{{$modal->tags}}</span> </p>
            </div>
            <div class="product-dis">
                <h3>{{$modal->name}}</h3>
                <hr>
                <p> {{ $modal->description}}</p>
                <form action="{{ route('post.add.cart', [$modal->uuid]) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-2 pr-0">
                            <input type="number" class="input-text qty text" step="1" min="1" max="50" name="quantity" value="1" title="Qty" size="4">
                        </div>
                        <div class="col-10">
                            <div>
                                <div class="row">
                                    <div class="col-6">
                                        <button class="btn btn-sm add-to-cart2">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-4 p-0">
                            <hr class="m-0 p-0">
                        </div>
                        <div class="pb-3 pt-3">
                            <div class="left-icon"> <a class="add-to-compare round-icon-btn" data-fancybox="gallery" data-src="#popup-1" href="javascript:;"> <i class="fa fa-eye" aria-hidden="true"></i> </a> <a href="#" class="mr-3"><i class="fa fa-balance-scale" aria-hidden="true"></i></a> </div>
                        </div>
                        <div class="col-md-12 mb-4 p-0">
                            <hr class="m-0 p-0">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>