@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class="pb-2">Update Password!</h2>
    </div>

    <div class="container bg-white">

        <form action="{{route('admin.submit.update_password')}}" method="post">
            @csrf

            <p>Enter old password</p>
            <input type="password" name="old_password" class="form-control" required>

            <p>Enter new password</p>
            <input type="password" name="password" class="form-control" required>

            <p>Comfirm new password</p>
            <input type="password" name="password_confirmation" class="form-control" required>

            <br>
            <button type="submit" class="btn btn-primary"> Update Password</button>
        </form>
    </div>
</div>
@endsection

@push('script')

@if(session()->has('success'))
<script>
    swal("{{session()->get('success')}}");
</script>
@endif


@endpush