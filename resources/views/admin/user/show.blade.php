@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class="pb-2">User <a href="{{route('admin.orders')}}" class="btn btn-sm btn-danger" style="float:right" onclick="window.close()">Close <i class="fa fa-times"></i></a></h2>
    </div>

    <br>
    <div class="container-full bg-white rounded" style="padding:30px;">

        <div class="row">
            <div class="col-md-4">

                <p><strong>User Name:</strong> {{ $data->first_name }} {{ $data->last_name }} </p>
                <p><strong>Address:</strong> {{ $data->address }} </p>
                <p><strong>State:</strong> {{ $data->state }} </p>
                <p><strong>Email:</strong> {{ $data->email }} </p>
                <p><strong>Phone:</strong> {{ $data->phone }} </p>
                <p><strong>Order Date:</strong> {{ $data->created_at }} </p>
                <p><strong>User Type:</strong> <button class="badge badge-success badge-pill">{{ucfirst($data->role)}} <i class="fas fa-check"></i></button></p>
                <p><strong>Email Verificaton:</strong>
                    @if($data->email_verified_at !==null)
                    <button class="badge badge-success badge-pill">Verified <i class="fas fa-check"></i></button>
                    @else
                    <button class="badge badge-danger badge-pill">Unverified <i class="fas fa-times"></i></button>
                    @endif
                </p>



                <br>
            </div>
            <div class="col-md-8">

                <h3>Update User Role</h3>
                <form action="{{ route('admin.role.update',[$data->id]) }}" method="post">
                    @csrf
                    <label for="status">Select Delivery statys</label>

                    <select class="form-control" name="role">
                        <option value="{{ $data->role }}">{{ ucfirst($data->role) }}</option>
                        <option value="customer">Customer</option>
                        <option value="admin">Admin</option>
                        <option value="retailer">Merchant</option>
                    </select>

                    <br>

                    <button class="btn btn-primary">Update</button>
                </form>
                <br>

                <h3>Customer Orders</h3>
                <table class="table">
                    <tr>
                        <th>S/N</th>
                        <th>Amount</th>
                        <th>Payment Status</th>
                        <th>Delivery Status</th>
                        <th>Show</th>
                    </tr>

                    @foreach($data->transactions as $key => $all)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$all->amount}}</td>
                        @if($all->status == true)
                        <td><button class="badge badge-success badge-pill">paid <i class="fas fa-check"></i></button></td>
                        @else
                        <td><button class="badge badge-danger badge-pill">unpaid <i class="fas fa-times"></i></button></td>
                        @endif

                        @if($all->delivery == 'pending')
                        <td><button class="badge badge-warning badge-pill">pending</button></td>
                        @elseif($all->delivery == 'processed')
                        <td> <button class="badge badge-primary badge-pill">processed</button></td>
                        @elseif($all->delivery == 'delivered')
                        <td> <button class="badge badge-success badge-pill">delivered</button></td>
                        @else
                        <td> <button class="badge badge-danger badge-pill">waiting</button></td>
                        @endif

                        <td><a href="{{ route('admin.order.find',[$all->id]) }}" class="btn btn-sm btn-primary">show <i class="fas fa-eye"></i></a></td>

                    </tr>
                    @endforeach

                    <tr>
                        <th colspan="3">Total Orders</th>
                        <th>{{ $data->transactions->count() }}</th>
                        <th></th>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>

@endsection

@push('script')


@endpush