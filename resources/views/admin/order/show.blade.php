@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class="pb-2">Orders <a href="{{route('admin.orders')}}" class="btn btn-sm btn-danger" style="float:right" onclick="window.close()">Close <i class="fa fa-times"></i></a></h2>
    </div>

    <br>
    <div class="container bg-white rounded" style="padding-top:30px; padding-bottom:30px;">

        <div class="row">
            <div class="col-md-4">

                <p><strong>Customer:</strong> {{ $data->user->first_name }} {{ $data->user->last_name }} </p>
                <p><strong>Address:</strong> {{ $data->user->address }} </p>
                <p><strong>State:</strong> {{ $data->user->state }} </p>
                <p><strong>Email:</strong> {{ $data->user->email }} </p>
                <p><strong>Phone:</strong> {{ $data->user->phone }} </p>
                <p><strong>Order Date:</strong> {{ $data->created_at }} </p>
                <p><strong>Payment method:</strong> {{ $data->payment_method }} </p>
                <p><strong>Payment Status:</strong>

                    @if($data->status == true)
                    <button class="badge badge-success badge-pill">successful</button>
                    @else
                    <button class="badge badge-danger badge-pill">pending</button>
                    @endif
                </p>

                <p><strong>Delivery Status:</strong>

                    @if($data->delivery == 'pending')
                    <button class="badge badge-warning badge-pill">pending</button>
                    @elseif($data->delivery == 'processed')
                    <button class="badge badge-primary badge-pill">processed</button>
                    @elseif($data->delivery == 'delivered')
                    <button class="badge badge-success badge-pill">delivered</button>
                    @else
                    <button class="badge badge-danger badge-pill">waiting</button>
                    @endif
                </p>

                <br>
            </div>
            <div class="col-md-8">
                <h3>Items ordered</h3>
                <table class="table">
                    <tr>
                        <th>S/N</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                    </tr>

                    @foreach($data->orders as $key => $order)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$order->product->name}}</td>
                        <td>{{$order->quantity}}</td>
                        <td>{{$order->price}}</td>
                    </tr>
                    @endforeach

                    <tr>
                        <th colspan="3">Total</th>
                        <th>{{ $data->orders->sum('price') }}</th>
                    </tr>
                </table>

                <br>
                <h3>Update Delivery Status</h3>
                <form action="{{ route('update.delivery',[$data->id]) }}" method="post">
                    @csrf
                    <label for="status">Select Delivery statys</label>

                    <select class="form-control" name="status">
                        <option value="{{ $data->delivery }}">{{ ucfirst($data->delivery) }}</option>
                        <option value="processed">Processed</option>
                        <option value="pending">Pending</option>
                        <option value="delivered">Delivered</option>
                    </select>

                    <br>

                    <button class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>

    </div>
</div>

@endsection

@push('script')


@endpush