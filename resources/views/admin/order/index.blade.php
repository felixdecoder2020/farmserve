@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline">Show Orders</h2>
        <div class="clearfix"></div>

    </div>
    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Payment Status</th>
                        <th>view</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $key => $all)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{$all->user->first_name}} {{$all->user->last_name}}</td>
                        <td>{{ number_format($all->amount, 2) }}</td>
                        <td>
                            @if($all->status == 1)
                            <button class="badge badge-success badge-sm">successful</button>
                            @else
                            <button class="badge badge-danger badge-sm">pending</button>
                            @endif



                        </td>
                        <td><a href="{{ route('admin.order.find',[$all->id]) }}" class="btn btn-sm btn-primary">view <i class="fas fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection