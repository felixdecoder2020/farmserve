@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline">Products</h2>
        <a href="{{ route('admin.product.create') }}" class="btn btn-primary float-right"> New Product <i class="fas fa-plus"></i></a>
        <div class="clearfix"></div>

    </div>

    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>price</th>
                        <th>Date</th>
                        <th>Edit</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $key => $all)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{$all->name}}</td>
                        <td>{{$all->price}}</td>
                        <td>{{ $all->created_at }}</td>
                        <td><a href="{{ route('admin.product.create.edit',[$all->id]) }}" class="btn btn-sm btn-primary">Edit <i class="fas fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection