@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class="pb-2">Update Product<a href="{{route('admin.products')}}" class="btn btn-sm btn-danger" style="float:right" onclick="window.close()">Close <i class="fa fa-times"></i></a></h2>
    </div>

    <br>
    <div class="container bg-white rounded" style="padding-top:30px; padding-bottom:30px;">

        <form action="{{ route('admin.product.create.update',[$data->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="name"> Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $data->name }}">
                    </div>
                    <div class="form-group">
                        <label for="price"> Price</label>
                        <input type="number" name="price" class="form-control" value="{{ $data->price }}">
                    </div>
                    <div class="form-group">
                        <label for="price"> Category</label>
                        <select name="category" class="form-control">
                            <option value="{{$data->category->id}}">{{$data->category->name}}</option>
                            @foreach($category as $all)
                            <option value="{{$all->id}}"> {{$all->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="price"> Quantity</label>
                        <input type="number" name="quantity" class="form-control" value="{{ $data->quantity }}">
                    </div>

                    <div class="form-group">
                        <label for="name"> Make product featured</label>
                        <br>
                        @if($data->type == 'featured')
                        <input type="radio" name="type" value="none"> None
                        <input type="radio" name="type" value="featured" checked style="margin-left: 20px"> Featured
                        <input type="radio" name="type" value="deal" style="margin-left: 20px"> Deal
                        @elseif($data->type == 'deal')
                        <input type="radio" name="type" value="none"> None
                        <input type="radio" name="type" value="featured" style="margin-left: 20px"> Featured
                        <input type="radio" name="type" value="deal" checked style="margin-left: 20px"> Deal
                        @else
                        <input type="radio" name="type" checked value="none"> None
                        <input type="radio" name="type" value="featured" style="margin-left: 20px"> Featured
                        <input type="radio" name="type" value="deal" style="margin-left: 20px"> Deal
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="price"> Description</label>
                        <textarea type="text" name="description" class="form-control" rows="5">{{ $data->description }}</textarea>
                    </div>
                </div>

                <div class="col-md-4-">
                    <div class="form-group">
                        <label for="name"> Select Main Picture</label>
                        <input type="file" name="photo[]" class="dropify" accept="image/png, image/gif, image/jpeg">
                    </div>
                    <div class="form-group">
                        <label for="name"> Select Picture</label>
                        <input type="file" name="photo[]" class="form-control" accept="image/png, image/gif, image/jpeg">
                    </div>
                    <div class="form-group">
                        <label for="name"> Select Picture</label>
                        <input type="file" name="photo[]" class="form-control" accept="image/png, image/gif, image/jpeg">
                    </div>

                    <div class="form-group">
                        <label for="name"> Select Picture</label>
                        <input type="file" name="photo[]" class="form-control" accept="image/png, image/gif, image/jpeg">
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <button type="submit" class="btn btn-primary"> Submit </button>
            </div>
        </form>
    </div>
    <div class="container bg-white rounded" style="padding-top:30px; padding-bottom:30px;">
        <h3>Photos of Product</h3>
        <br>
        <div class="row">
            @foreach($data->photos as $photo)
            <div class="col-md-4">
                <form action="{{ route('remove.photo',[$photo->id]) }}" method="post">
                    @csrf
                    <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                </form>
                <img src="{{ asset($photo->image) }}" width="150px" />
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('.dropify').dropify();
    });
</script>

@endpush