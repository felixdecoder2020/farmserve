@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class="pb-2">Create Product<a href="{{route('admin.products')}}" class="btn btn-sm btn-danger" style="float:right" onclick="window.close()">Close <i class="fa fa-times"></i></a></h2>
    </div>

    <br>
    <div class="container bg-white rounded" style="padding-top:30px; padding-bottom:30px;">

        <form action="{{ route('admin.product.create.submit')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="name"> Name</label>
                        <input type="text" name="name" class="form-control" value="{{ old('name')}}">
                    </div>
                    <div class="form-group">
                        <label for="price"> Price</label>
                        <input type="number" name="price" class="form-control" value="{{ old('price')}}">
                    </div>
                    <div class="form-group">
                        <label for="price"> Category</label>
                        <select name="category" class="form-control">
                            <option value="">--Select Option --</option>
                            @foreach($data as $all)
                            <option value="{{$all->id}}"> {{$all->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="price"> Quantity</label>
                        <input type="number" name="quantity" class="form-control" value="{{ old('quantity')}}">
                    </div>
                    <div class="form-group">
                        <label for="price"> Description</label>
                        <textarea type="text" name="description" class="form-control" rows="5">{{ old('description')}}</textarea>
                    </div>
                </div>

                <div class="col-md-4-">
                    <div class="form-group">
                        <label for="name"> Select Main Picture</label>
                        <input type="file" name="photo[]" class="dropify" accept="image/png, image/gif, image/jpeg" required>
                    </div>
                    <div class="form-group">
                        <label for="name"> Select Picture</label>
                        <input type="file" name="photo[]" class="form-control" accept="image/png, image/gif, image/jpeg">
                    </div>
                    <div class="form-group">
                        <label for="name"> Select Picture</label>
                        <input type="file" name="photo[]" class="form-control" accept="image/png, image/gif, image/jpeg">
                    </div>

                    <div class="form-group">
                        <label for="name"> Select Picture</label>
                        <input type="file" name="photo[]" class="form-control"  accept="image/png, image/gif, image/jpeg">
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <button type="submit" class="btn btn-primary"> Submit </button>
            </div>
        </form>
    </div>
</div>

@endsection

@push('script')

@if(session()->has('success'))
<script>
    swal("{{session()->get('success')}}")
</script>
@endif
<script>
    $(document).ready(function() {
        $('.dropify').dropify();
    });
</script>

@endpush