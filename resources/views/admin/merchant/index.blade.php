@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline">Merchant</h2>
        <div class="clearfix"></div>

    </div>

    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>State</th>
                        <th>Phone</th>
                        <th>email</th>
                        <th>Date</th>
                        <th>Edit</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $key => $all)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{$all->first_name}} {{$all->last_name}}</td>
                        <td>{{$all->state}}</td>
                        <td>{{$all->phone}}</td>
                        <td>{{$all->email}}</td>
                        <td>{{ $all->created_at }}</td>
                        <td><a href="{{ route('admin.merchant.single',[$all->id]) }}" class="btn btn-sm btn-primary">view <i class="fas fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection