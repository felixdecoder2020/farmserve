@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline">Request Withdrawal</h2>
        <a href="#" data-toggle="modal" data-target="#addData" class="btn btn-primary float-right"> Request Withdrawal <i class="fas fa-plus"></i></a>
        <div class="clearfix"></div>

    </div>

    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>Bank Name</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($withdrawals as $key => $data)
                    <tr>
                        <td>{{$key +1}}</td>
                        <td>{{$data->account_name}}</td>
                        <td>{{$data->account_number}}</td>
                        <td>{{$data->bank_name}}</td>
                        <td>{{$data->amount}}</td>
                        <td>

                            @if($data->status == 'successful')
                            <button class="badge badge-success badge-sm">successful</button>
                            @elseif($data->status == 'declined')
                            <button class="badge badge-danger badge-sm">declined</button>
                            @else
                            <button class="badge badge-warning badge-sm">pending</button>
                            @endif

                        </td>
                        <td>{{ $data->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Withdrawal Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.withdrawals.post') }}" method="post">
                @csrf
                <div class="modal-body">
                    <p>Input Amount</p>
                    <input type="number" name="amount" class="form-control" style="border-color: blue;">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection