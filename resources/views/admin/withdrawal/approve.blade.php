@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline"> Withdrawal Requests</h2>
        <div class="clearfix"></div>

    </div>

    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>Bank Name</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Approve</th>
                        <th>Reject</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($withdrawals as $key => $data)
                    <tr>
                        <td>{{$key +1}}</td>
                        <td>{{$data->account_name}}</td>
                        <td>{{$data->account_number}}</td>
                        <td>{{$data->bank_name}}</td>
                        <td>{{$data->amount}}</td>
                        <td>
                            @if($data->status == 'successful')
                            <button class="badge badge-success badge-sm">successful</button>
                            @elseif($data->status == 'declined')
                            <button class="badge badge-danger badge-sm">declined</button>
                            @else
                            <button class="badge badge-warning badge-sm">pending</button>
                            @endif
                        </td>
                        <td>{{ $data->created_at }}</td>
                        <form action="{{ route('admin.withdrawals.manage',[$data->id]) }}" method="post">
                            @csrf
                            <td> <button type="submit" name="success" value="success" class="badge badge-success badge-sm">successful</button></td>
                            <td><button type="submit" name="decline" value="decline" class="badge badge-danger badge-sm">declined</button></td>
                        </form>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection