@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline">Bank Details</h2>
        <a href="#" data-toggle="modal" data-target="#addData" class="btn btn-primary float-right"> Update Bank <i class="fas fa-plus"></i></a>
        <div class="clearfix"></div>

    </div>

    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Bank Name</th>
                        <th>Account Number</th>
                        <th>Account Type</th>
                        <th>Date</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        @if($data)
                        <td>{{$data->account_name}}</td>
                        <td>{{$data->bank->name}}</td>
                        <td>{{$data->account_number}}</td>
                        <td>{{$data->account_type}}</td>
                        <td>{{ $data->created_at }}</td>
                        <td><a href="#" data-toggle="modal" data-target="#addData" class="btn btn-sm btn-primary">Edit <i class="fas fa-pencil"></i></a></td>
                        @else
                        <td colspan="2">You have not added bank account</td>
                        @endif
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.bank.create') }}" method="post">
                @csrf
                <div class="modal-body">
                    <p>Select Bank</p>
                    <select name="bank" class="form-control" placeholder="E.g Agbodo" style="border-color: blue;">
                        @if(Auth::user()->userBank)
                        <option value="{{ Auth::user()->userBank->bank->id}}">{{ Auth::user()->userBank->bank->name}} </option>
                        @else
                        <option value="">-- Select Bank -- </option>
                        @endif

                        @foreach($banks as $bank )
                        <option value="{{$bank->id}}">{{$bank->name}}</option>
                        @endforeach
                    </select>

                    <p>Input Account number</p>
                    <input type="text" name="account_number" class="form-control" value="{{ Auth::user()->userBank->account_number ?? null }}" style="border-color: blue;">

                    <p>Input Account name</p>
                    <input type="text" name="account_name" class="form-control" value="{{ Auth::user()->userBank->account_name ?? null }}" style="border-color: blue;">


                    <p>Select Account type</p>
                    <select name="account_type" class="form-control" style="border-color: blue;">
                        @if(Auth::user()->userBank)
                        <option value="{{ Auth::user()->userBank->account_type}}">{{ ucwords(Auth::user()->userBank->account_type) }} Account </option>
                        @else
                        <option value="">-- Select type -- </option>
                        @endif
                        <option value="">-- Select Bank -- </option>
                        <option value="savings">Savings Account </option>
                        <option value="current">Current Account </option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection