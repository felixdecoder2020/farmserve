@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class="pb-2">Update {{$data->name}} Category <a href="{{route('category')}}" class="btn btn-sm btn-danger" style="float:right" onclick="window.close()">Close <i class="fa fa-times"></i></a></h2>
    </div>

    <br>
    <div class="container bg-white rounded" style="padding-top:30px; padding-bottom:30px;">

        <form action="{{ route('category.update.post',[$data->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="col-md-8">
                <div class="form-group">
                    <label for="name"> Name</label>
                    <input type="text" name="name" class="form-control" value="{{$data->name}}">
                </div>

                <div class="form-group">
                    <label for="name"> Make featured category</label>
                    <br>
                    @if($data->featured == true)
                    <input type="radio" name="featured" value="1" checked> True
                    <input type="radio" name="featured" value="0" style="margin-left: 20px"> False
                    @else
                    <input type="radio" name="featured" value="1"> True
                    <input type="radio" name="featured" value="0" checked style="margin-left: 20px"> False
                    @endif
                </div>

                <div class="form-group">
                    <label for="name"> Select background(Optional if not featured category)</label>
                    <br>
                    <input type="file" name="background" class="dropify" data-default-file="{{ asset($data->background) }}">
                </div>
            </div>

            <div class="col-md-12">
                <button type="submit" class="btn btn-primary"> Update </button>
            </div>
        </form>
    </div>
</div>

@endsection

@push('script')

@if(session()->has('success'))
<script>
    swal("{{session()->get('success')}}")
</script>
@endif
<script>
    $(document).ready(function() {
        $('.dropify').dropify();
    });
</script>

@endpush