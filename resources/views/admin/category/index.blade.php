@extends('layouts.dashboard.app')
@section('content')
<div class="page-inner">
    <div class="mt-2 mb-4">
        <h2 class=" pb-2" style="display:inline">Categories</h2>
        <a href="#" data-toggle="modal" data-target="#addData" class="btn btn-primary float-right"> New Category <i class="fas fa-plus"></i></a>
        <div class="clearfix"></div>

    </div>

    <div class="container-full">

        <div class="table-responsive">
            <table class="table table-stripped table-hovered" id="myTable">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Edit</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $key => $all)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{$all->name}}</td>
                        <td>{{ $all->created_at }}</td>
                        <td><a href="{{ route('category.update',[$all->id]) }}" class="btn btn-sm btn-primary">Edit <i class="fas fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('category.add') }}" method="post">
                @csrf
                <div class="modal-body">
                    <p>Enter Category Name</p>
                    <input type="text" name="name" class="form-control" placeholder="E.g Agbodo" style="border-color: blue;">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection