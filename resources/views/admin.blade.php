@extends('layouts.dashboard.app')
@section('content')
<div class="panel-header">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="pb-2 fw-bold">Dashboard</h2>
                <h5 class="op-7 mb-2">Welcome! {{ Auth::user()->first_name }} to {{env('APP_NAME')}} admin dashboard</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{ route('admin.products') }}" class="btn text-success bg-white btn-round mr-2"><i class="fas fa-link mr-1"></i>Upload Product</a>
                <a href="{{ route('admin.orders') }}" class="btn btn-danger btn-round"><i class="fas fa-paper-plane mr-1"></i>View Orders</a>

            </div>
        </div>
    </div>
</div>
<div class="page-inner mt--5">

    <div class="row">
        @if(Auth::user()->role == 'admin')
        <div class="col-sm-6 col-md-2">
            <div class="card card-stats card-round">
                <div class="card-body ">
                    <div class="row align-items-center">
                        <div class="col-icon">
                            <div class="icon-big text-center icon-primary bubble-shadow-small">
                                <i class="flaticon-users"></i>
                            </div>
                        </div>
                        <div class="col col-stats ml-3 ml-sm-0">
                            <div class="numbers">
                                <p class="card-category">Customers</p>
                                <h4 class="card-title">{{ $data["total_customers"] }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-2">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-icon">
                            <div class="icon-big text-center icon-warning bubble-shadow-small">
                                <i class="flaticon-graph"></i>
                            </div>
                        </div>
                        <div class="col col-stats ml-3 ml-sm-0">
                            <div class="numbers">
                                <p class="card-category">Retailers</p>
                                <h4 class="card-title">{{ $data["total_retailers"] }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="col-sm-6 col-md-2">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-icon">
                            <div class="icon-big text-center icon-warning bubble-shadow-small">
                                <i class="flaticon-interface-6"></i>
                            </div>
                        </div>
                        <div class="col col-stats ml-3 ml-sm-0">
                            <div class="numbers">
                                <p class="card-category">Total Sales</p>
                                <h4 class="card-title">{{ $data["total_sales"]  }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-icon">
                            <div class="icon-big text-center icon-success bubble-shadow-small">
                                <i class="flaticon-graph"></i>
                            </div>
                        </div>
                        <div class="col col-stats ml-3 ml-sm-0">
                            <div class="numbers">
                                <p class="card-category">Total Products</p>
                                <h4 class="card-title">{{ $data["total_products"] }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-icon">
                            <div class="icon-big text-center icon-info bubble-shadow-small">
                                <i class="flaticon-graph"></i>
                            </div>
                        </div>
                        <div class="col col-stats ml-3 ml-sm-0">
                            <div class="numbers">
                                <p class="card-category">Withdrawal</p>
                                <h4 class="card-title">{{ $data["withdrawals"] }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Weekly sales Report (Previous week)
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-container">
                        <canvas id="lineChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('script')

<script>
    const ctx = document.getElementById('lineChart').getContext('2d');

    const lineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: JSON.parse(`<?php echo $days;  ?>`),
            datasets: [{
                label: '# of Income',
                data: JSON.parse(`<?php echo $days_data;  ?>`),
                backgroundColor: [
                    'rgba(244, 176, 90, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>

@endpush