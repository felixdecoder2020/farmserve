@extends("layouts.home")
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

@endpush
@section('contents')

<!-- Navigation -->
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb2">
            <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
            <li class="breadcrumb-item">Cart</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-12 col-xl-8 mb-4">
            <div class="table-responsive cart-table table-borderless">
                <form action="{{ route('cart.update') }}" method="post">
                    @csrf
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Product</th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity</th>
                                <th>Total</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $total = 0;
                            @endphp
                            @foreach($data as $key => $cart)
                            <tr>
                                <td class="product-thumbnail text-center"><a href="#"><img src="{{ asset($cart->product->photos[0]->image) }}" class="" alt=""></a></td>
                                <td>
                                    <div class="cart-detail">{{$cart->product->name}}.</div>
                                </td>
                                <td class="text-center">
                                    <div style="width:80px"> &#x20A6; {{ number_format($cart->product->price,2) }} </div>
                                </td>
                                <td class="product-quantity" data-title="Quantity">
                                    <div class="input-group"> <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity[{{$key}}]"> <i class="fa fa-minus"></i> </button>
                                        </span>
                                        <input type="hidden" name="product[]" value="{{ $cart->uuid }}">
                                        <input type="text" name="quantity[{{$key}}]" class="form-control input-number" value="{{ $cart->quantity }}" min="1" max="100" required>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity[{{$key}}]"> <i class="fa fa-plus"></i> </button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    @php
                                    $total += $cart->product->price*$cart->quantity;
                                    @endphp
                                    <div style="width:100px">
                                        &#x20A6; {{ number_format($cart->product->price*$cart->quantity,2) }}
                                </td>
                                <td><a href="{{ route('cart.delete.item',[$cart->uuid]) }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr>
                            @endforeach

                            <tr>
                                <td colspan="6">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="coupon">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-6 pr-0">
                                                            <input type="text" name="coupon_code" class="input-text form-control coupon_code mb-1 mt-1" id="" value="" placeholder="Coupon code">
                                                        </div>
                                                        <div class="col-md-6"> <a href="#" class="btn cart mb-1 mt-1" value="Apply coupon">Apply coupon</a> </div>
                                                    </div>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td valign="top" class="text-right"><button class="btn cart">Update cart</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </form>
            </div>
        </div>
        <div class="col-12 col-xl-4 mb-5">
            <div class="cart_totals">
                <div class="table-responsive">
                    <table cellspacing="0" class="table table-borderless mb-0">
                        <tbody>
                            <tr class="cart-subtotal">
                                <td>Subtotal</td>
                                <td class="text-right"> &#x20A6;{{ number_format( $total,2) }}</td>
                            </tr>
                            <tr class="shipping">
                                <td colspan="2" align="left" class="mb-0 pb-0">
                                    <h5 class="m-0 p-0">Shipping</h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="flat-rate">
                                    <h5>Flat rate:</h5>
                                </td>
                                <td class="text-right amount">
                                    @php
                                    $shipping = (10/100)*( $total);
                                    @endphp
                                    &#x20A6; {{ number_format($shipping,2) }}

                                </td>
                            </tr>
                            <tr class="order-total">
                                <td>
                                    <h5>Total</h5>
                                </td>
                                <td align="right"> &#x20A6; {{ number_format( $total+$shipping,2) }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <form action="{{ route('cart.checkout') }}" method="post">
                        @csrf
                        <button class="btn cart w-100"> Proceed to checkout</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('assets/vendor/jquery/number-plsu-min.js') }}" type="text/javascript"></script>
@endpush