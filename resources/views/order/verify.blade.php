@extends("layouts.home")
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

@endpush
@section('contents')

<!-- Navigation -->
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb2">
            <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
            <li class="breadcrumb-item">Payment verification</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-12 col-xl-8 mb-4">
            <div class="table-responsive cart-table table-borderless">
                <form action="{{ route('cart.update') }}" method="post">
                    @csrf
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Product</th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity</th>
                                <th>Total</th>

                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $total = 0;
                            @endphp
                            @foreach($data as $key => $order)
                            <tr>
                                <td class="product-thumbnail text-center"><a href="#"><img src="{{ asset($order->product->photos[0]->image) }}" class="" alt=""></a></td>
                                <td>
                                    <div class="cart-detail">{{$order->product->name}}.</div>
                                </td>
                                <td class="text-center">
                                    <div style="width:80px"> &#x20A6; {{ number_format($order->product->price,2) }} </div>
                                </td>
                                <td class="text-center" data-title="Quantity"> {{$order->quantity}} </td>
                                <td>
                                    @php
                                    $total += $order->price;
                                    @endphp
                                    <div style="width:100px">
                                        &#x20A6; {{ number_format($order->price,2) }}
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </form>
            </div>
        </div>
        <div class="col-12 col-xl-4 mb-5">
            <div class="cart_totals">
                <div class="table-responsive">
                    <table cellspacing="0" class="table table-borderless mb-0">
                        <tbody>
                            <tr class="cart-subtotal">
                                <td>Payment Status</td>
                                <td class="text-right">
                                    @if($payment->status == 1)
                                    <button class="badge badge-success badge-pill" style="border:0; padding: 7px">successful</button>
                                    @else
                                    <button class="badge badge-danger badge-pill" style="border:0; padding: 7px">pending</button>
                                    @endif
                                </td>
                            </tr>
                            <tr class="cart-subtotal">
                                <td>Delivery Status</td>
                                <td class="text-right">
                                    @if($payment->delivery == 'pending')
                                    <button class="badge badge-warning badge-pill" style="border:0; padding: 7px">pending</button>
                                    @elseif($payment->delivery == 'processed')
                                    <button class="badge badge-primary badge-pill" style="border:0; padding: 7px">processed</button>
                                    @elseif($payment->delivery == 'delivered')
                                    <button class="badge badge-success badge-pill" style="border:0; padding: 7px">delivered</button>
                                    @else
                                    <button class="badge badge-danger badge-pill" style="border:0; padding: 7px">waiting</button>
                                    @endif
                                </td>
                            </tr>
                            <tr class="cart-subtotal">
                                <td>Subtotal</td>
                                <td class="text-right"> &#x20A6;{{ number_format($total,2) }}</td>
                            </tr>
                            <tr class="shipping">
                                <td colspan="2" align="left" class="mb-0 pb-0">
                                    <h5 class="m-0 p-0">Shipping</h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="flat-rate">
                                    <h5>Flat rate:</h5>
                                </td>
                                <td class="text-right amount">
                                    @php
                                    $shipping = (10/100)*( $total);
                                    @endphp
                                    &#x20A6; {{ number_format($shipping,2) }}

                                </td>
                            </tr>
                            <tr class="order-total">
                                <td>
                                    <h5>Total</h5>
                                </td>
                                <td align="right"> &#x20A6; {{ number_format( $payment->amount,2) }}</td>
                            </tr>

                            <tr class="cart-subtotal">
                                <td></td>
                                <td class="text-right">
                                    <a href="{{ route('dashboard') }}" class="badge badge-info badge-pill" style="border:0; padding: 7px">Go to Dashboard</a>
                                </td>
                            </tr>
                            <tr class="cart-subtotal">
                                <td>Delivery Address</td>
                                <td class="text-right">
                                    <p>{{Auth::user()->address}} - {{Auth::user()->state}}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection