@extends("layouts.home")
@section('contents')
<!-- Navigation -->

<div class="container search-div">
    <form action="{{ route('filter.search') }}">
        <div class="row">
            <div class="col-lg-3 col-md-4 top-dropdown">
                <div class="all-cate custom-select2">
                    <select class="category_left select-selected2" name="category">
                        <option value="all">Filter By Category</option>
                        @foreach(categories() as $data)
                        <option value="{{$data->name}}">{{$data->name}}</option>
                        @endforeach
                    </select>


                </div>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="input-group filter-by">

                    <input type="text" class="form-control" name="query" placeholder="What do you need?" required>
                    <span class="input-group-btn">
                        <button class="btn btn-default search-bt" type="submit">SEARCH</button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb2 breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Search</a></li>
            <li class="breadcrumb-item">{{$query}}</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="row">
                <div class="col-12">
                    <div class="right-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Shop</h3>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="products" class="row view-group">

                        @foreach($product as $data)
                        <div class="item col-md-4 col-lg-3 mb-4">
                            <!-- <div class="sale-flag-side"> <span class="sale-text">Sale</span> </div> -->
                            <div class="thumbnail card product">
                                <div class="img-event"> <a class="group list-group-image" href="{{ route('customer.product.show', [$data->uuid]) }}"> <img class="img-fluid" src="{{ (isset($data->photos[0])) ? asset($data->photos[0]->image) : null }}" alt=""></a> </div>
                                <div class=" caption card-body">
                                    <h5 class="product-type">{{$data->category->name}}</h5>
                                    <h3 class="product-name">{{$data->name}}</h3>
                                    <div class="row m-0 list-n">
                                        <div class="col-12 p-0">
                                            <h3 class="product-price"> &#x20A6;{{ number_format($data->price,2) }}</h3>
                                        </div>
                                        <div class="col-12 p-0">
                                            <div class="product-price">
                                                <form action="{{ route('post.add.cart', [$data->uuid]) }}" method="post" class="form-inline">
                                                    @csrf
                                                    <div class="stepper-widget">
                                                        <button type="button" class="js-qty-down">-</button>
                                                        <input type="text" class="js-qty-input" value="1" name="quantity" />
                                                        <button type="button" class="js-qty-up">+</button>
                                                        <button type="submit" class="add2"><i class="fa fa-shopping-bag" aria-hidden="true"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-select">
                                        <button data-toggle="tooltip" data-placement="top" title="Quick view" class="add-to-compare round-icon-btn" data-fancybox="gallery" data-src="#product_details{{$data->id}}" href="javascript:;"> <i class="fa fa-eye" aria-hidden="true"></i> </button>
                                        <button data-toggle="tooltip" data-placement="top" title="Add to cart" onClick="window.location.href='{{ route("cart.add.item",[$data->uuid]) }}'" class="add-to-wishlist round-icon-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i></button>
                                        @include('includes.modal', ['address'=> 'product_details'.$data->id, 'modal' => $data])
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

@endsection

@push('scripts')
<link rel="stylesheet" href="{{ asset('assets/vendor/price_range/jquery-ui.css" type="text/css') }}" media="all" />
<script src="{{ asset('assets/vendor/price_range/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/price_range/price_range_script.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/price_range/price_range_style.css') }}" />

<script src="{{ asset('assets/vendor/stepper/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/stepper/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/stepper.widget.js') }}"></script>
@endpush