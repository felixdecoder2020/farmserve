@extends("layouts.home")
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

@endpush
@section('contents')
<!-- Navigation -->
<div class="container search-div">
    <form action="{{ route('filter.search') }}">
        <div class="row">
            <div class="col-lg-3 col-md-4 top-dropdown">
                <div class="all-cate custom-select2">
                    <select class="category_left select-selected2" name="category">
                        <option value="all">Filter By Category</option>
                        @foreach(categories() as $data)
                        <option value="{{$data->name}}">{{$data->name}}</option>
                        @endforeach
                    </select>


                </div>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="input-group filter-by">

                    <input type="text" class="form-control" name="query" placeholder="What do you need?" required>
                    <span class="input-group-btn">
                        <button class="btn btn-default search-bt" type="submit">SEARCH</button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Navigation -->
<div class="container">
    <nav aria-label="breadcrumb" class="bread-boder">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                    <li class="breadcrumb-item">Shop</li>
                </ol>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="custom-select2">
                            <select>
                                <option>Default Sorting</option>
                                <option value="A-Z">A to Z</option>
                                <option value="Z-A">Z to A</option>
                                <option value="High to low price">High to low price</option>
                                <option value="Low to high price">Low to high</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="custom-select2">
                            <select>
                                <option value="A-Z">Show 10</option>
                                <option value="Z-A">Show 20</option>
                                <option value="High to low price">Show 30</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </nav>
    <div class="row">
        <div class="col-lg-3 col-md-12">
            <div class="inner-left-menu">
                <h3>Categories</h3>
                <div class="list-css">
                    <ul>

                        @foreach(categories() as $data)
                        <li><a href="{{route('filter.category',[$data->name]) }}">{{$data->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <h3>Filter By Price</h3>
                <div class="price-range-block">
                    <div id="slider-range" class="price-filter-range"></div>
                    <div class="row">
                        <div class="col-9 p-0">
                            <input type="number" min=0 max="9900" oninput="validity.valid||(value='0');" id="min_price" class="price-range-field" />
                            <input type="number" min=0 max="10000" oninput="validity.valid||(value='10000');" id="max_price" class="price-range-field" />
                        </div>
                        <div class="col-3 p-0">
                            <button type="button" class="btn btn-filter">Filter</button>
                        </div>
                    </div>
                    <br>
                </div>
                <h3>Popular Picks</h3>
                <div class="list-css">
                    <ul>
                        <li>
                            <!-- Default unchecked -->
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked-1">
                                <label class="custom-control-label" for="defaultUnchecked-1">Top Sales</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked-2">
                                <label class="custom-control-label" for="defaultUnchecked-2">New Products</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked-3">
                                <label class="custom-control-label" for="defaultUnchecked-3">Featured Products</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked-4">
                                <label class="custom-control-label" for="defaultUnchecked-4">Bestsellers</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <h3>Product Tags</h3>
                <div class="tag_bottom"><a class="tag-btn" href="#">organic</a><a class="tag-btn" href="shop_grid%2blist_3col.html">vegatable</a><a class="tag-btn" href="#">fruits</a><a class="tag-btn" href="#">fresh meat</a><a class="tag-btn" href="#">fastfood</a><a class="tag-btn" href="#">natural</a></div>
            </div>
        </div>
        <div class="col-lg-9 col-md-12">
            <div class="row">
                <div class="col-12">
                    <div class="right-heading">
                        <div class="row">
                            <div class="col-md-4 col-4">
                                <h3>Shop Grid</h3>
                            </div>
                            <div class="col-md-8 col-8">
                                <div class="product-filter">
                                    <div class="view-method"> <a href="#" id="grid"><i class="fa fa-th-large"></i></a> <a href="#" id="list"><i class="fa fa-list"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="products" class="row view-group">

                        @foreach($product as $data)
                        <div class="item col-lg-4 col-md-4 mb-4 mb-4">
                            <!-- <div class="sale-flag-side"> <span class="sale-text">Sale</span> </div> -->
                            <div class="thumbnail card product">
                                <div class="img-event"> <a class="group list-group-image" href="{{ route('customer.product.show', [$data->uuid]) }}"> <img class="img-fluid" src="{{ (isset($data->photos[0])) ? asset($data->photos[0]->image) : null }}" alt=""></a> </div>
                                <div class="caption card-body">
                                    <h5 class="product-type">{{$data->category->name}}</h5>
                                    <h3 class="product-name">{{$data->name}}</h3>

                                    <div class="row m-0 list-n">
                                        <div class="col-12 p-0">
                                            <h3 class="product-price"> &#x20A6;{{ number_format($data->price,2) }}</h3>
                                        </div>
                                        <div class="col-12 p-0">
                                            <div class="product-price">
                                                <form action="{{ route('post.add.cart', [$data->uuid]) }}" method="post" class="form-inline">
                                                    @csrf
                                                    <div class="stepper-widget">
                                                        <button type="button" class="js-qty-down">-</button>
                                                        <input type="text" class="js-qty-input" value="1" name="quantity" />
                                                        <button type="button" class="js-qty-up">+</button>
                                                        <button type="submit" class="add2"><i class="fa fa-shopping-bag" aria-hidden="true"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-select">
                                        <button data-toggle="tooltip" data-placement="top" title="Quick view" class="add-to-compare round-icon-btn" data-fancybox="gallery" data-src="#product_details{{$data->id}}" href="javascript:;"> <i class="fa fa-eye" aria-hidden="true"></i> </button>
                                        <button data-toggle="tooltip" data-placement="top" title="Add to cart" onClick="window.location.href='{{ route("cart.add.item",[$data->uuid]) }}'" class="add-to-wishlist round-icon-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i></button>
                                        @include('includes.modal', ['address'=> 'product_details'.$data->id, 'modal' => $data])
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="clearfix"></div>
                        <!-- Pagination -->
                        <div class="text-center col">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination pagination-template d-flex justify-content-center float-none">
                                    <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-left"></i></a></li>
                                    <li class="page-item"><a href="#" class="page-link active">1</a></li>
                                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                                    <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

@endsection

@push('scripts')
<link rel="stylesheet" href="{{ asset('assets/vendor/price_range/jquery-ui.css" type="text/css') }}" media="all" />
<script src="{{ asset('assets/vendor/price_range/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/price_range/price_range_script.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/price_range/price_range_style.css') }}" />

<script src="{{ asset('assets/vendor/stepper/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/stepper/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/stepper.widget.js') }}"></script>
@endpush