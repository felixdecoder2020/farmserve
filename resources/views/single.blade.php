@extends("layouts.home")
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/detail-page/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery/easyzoom.css') }}">

@endpush
@section('contents')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb2 breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
            <li class="breadcrumb-item">Single Product</li>
        </ol>
    </nav>
    <div class="clearfix"></div>
</div>
<div class="inner-header2">
    <h3>{{$data->name}}</h3>
</div>
<div class="inner-page">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-6"> <a href="#" class="wish-list"><i class="fa fa-heart" aria-hidden="true"></i></a>

                <div id="sync1" class="owl-carousel owl-theme">
                    @foreach($data->photos as $photo)
                    <div class="item easyzoom easyzoom--overlay"> <a href="{{ asset($photo->image) }}"> <img src="{{ asset($photo->image) }}" alt="" title="" /> </a> </div>
                    @endforeach
                </div>

                <div id="sync2" class="owl-carousel owl-theme">
                    @foreach($data->photos as $key => $photo)

                    <div class="item"><img src="{{ asset($photo->image) }}" alt="" title=""></div>
                    @endforeach
                </div>



            </div>
            <div class="col-lg-6  product-text">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6">
                        <h3>{{$data->name}}</h3>
                        <img src="{{ asset('assets/images/star.png') }}" alt="" title=""> <img src="{{ asset('assets/images/star.png') }}" alt="" title=""> <img src="{{ asset('assets/images/star.png') }}" alt="" title=""> <img src="{{ asset('assets/images/star.png') }}" alt="" title=""> <img src="{{ asset('assets/images/star.png') }}" alt="" title="">
                    </div>
                    <div class="col-md-6 col-sm-6 text-right col-6">
                        <div class="price-css">

                            <!-- <span>$35.00</span> -->
                            <div class="clearfix"></div>
                            &#x20A6; {{ number_format($data->price,2) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mt-3">
                            <p>{{ $data->description}}</p>
                            <div class="mt-3 text-2">
                                <p><span>Availability</span>: &nbsp;&nbsp;
                                    @if($data->quantity < 1) <img src="{{ asset('assets/images/unavailable.png') }}" alt="" title=""> Out of stock</p>
                                @else
                                <img src="{{ asset('assets/images/available.png') }}" alt="" title=""> In Stock</p>
                                @endif

                                <p><span>Vendor</span>: &nbsp;&nbsp;{{$data->user->first_name}} </p>
                                <p><span>Product Type</span>: &nbsp;&nbsp;{{$data->category->name}} </p>
                            </div>
                            <div class="quality">
                                <form action="{{ route('post.add.cart', [$data->uuid]) }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group">
                                                <h4>Quality :</h4>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity"> <i class="fa fa-minus"></i> </button>
                                                </span>
                                                <input type="number" name="quantity" class="input-number" value="1" min="1" max="100">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity"><i class="fa fa-plus"></i> </button>
                                                </span>
                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <button class="btn add-to-cart2">Add To Cart</button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            <div class="share">
                                <h3 class="pull-left">Share : &nbsp; &nbsp;</h3>
                                <div class="pull-left">
                                    <ul class="social-network3">
                                        <li><a href="#" class="facebook-icon" title=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" class="twitter-icon" title=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" class="google-icon" title=""><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#" class="linkedin-icon" title=""><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row categories">
                                <div class="col-md-7">
                                    <h3 class="pull-left"> Categories : <span>&nbsp;{{$data->category->name}} </span></h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="pull-left"> Tags : <span>&nbsp;items, quality, nice</span></h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="list-3 row">
                                    <div class="col-lg-4"><img src="assets/images/shield.png" alt=""> 10 days return</div>
                                    <div class="col-lg-4"><img src="assets/images/shipping.png" alt=""> Quick Delivery</div>
                                    <div class="col-lg-4"><img src="assets/images/transfer.png" alt=""> 35% Cashback</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
            <div class="col-md-12">
                <div id="tabs" class="description">
                    <div>
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist"> <a class="active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>&nbsp;|&nbsp;<a class="" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Additional information</a>&nbsp;|&nbsp;<a class="" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reviews</a> </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active text-1" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <p class="p1"><strong>Production Description <br>
                                    </strong>
                                    {{$data->description}}
                                    <!-- snippet location product_description -->
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th width="20%">
                                                <div align="left">Brand</div>
                                            </th>
                                            <td>{{$data->category->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div align="left">Quantity in stock</div>
                                            </th>
                                            <td> {{$data->quantity}}</td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div align="left">Product classification</div>
                                            </th>
                                            <td>{{ $data->type}}</td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div align="left">Storage life</div>
                                            </th>
                                            <td>1 year</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row m-0 text-center-m">
                                            <div class="col-lg-2 col-md-3 col-sm-2 text-center mb-3"><img src="assets/images/review1.jpg" alt="" title="" class="radius image-boder img-fluid"></div>
                                            <div class="col-lg-10 col-md-9 col-sm-10">
                                                <h2 class="font-15 mt-10">Daryl Michaels <span class="font-13 text-themecolor">Product: Mobile Phone</span></h2>
                                                <span class="fa fa-star checked font-13"></span> <span class="fa fa-star checked font-13"></span> <span class="fa fa-star checked font-13"></span> <span class="fa fa-star font-13"></span> <span class="fa fa-star font-13"></span> &nbsp;<span class="red">1 Min ago </span>
                                                <div class="mt-1">
                                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-12">
                                                <hr>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-2 col-md-3 col-sm-2 text-center"><img src="assets/images/review2.jpg" alt="" title="" class="radius image-boder img-fluid"></div>
                                            <div class="col-lg-10 col-md-9 col-sm-10">
                                                <h2 class="font-15 mt-10">Daryl Michaels <span class="font-13 text-themecolor">Product: Mobile Phone</span></h2>
                                                <span class="fa fa-star checked font-13"></span> <span class="fa fa-star checked font-13"></span> <span class="fa fa-star checked font-13"></span> <span class="fa fa-star font-13"></span> <span class="fa fa-star font-13"></span> <span class="red">&nbsp;1 Min ago </span>
                                                <div class="mt-1">
                                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Write Your Review</h4>
                                        <p class="mb-3">Your email address will not be published.</p>
                                        <form class="review-form">
                                            <div class="form-group">
                                                <input class="form-control" type="text" placeholder="Your Name">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="email" placeholder="Email Address">
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" rows="5" placeholder="Your review"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-rating"> <strong>Your Rating:</strong>
                                                    <div class="stars">
                                                        <input type="radio" id="star5" name="rating" value="5">
                                                        <label for="star5"></label>
                                                        <input type="radio" id="star4" name="rating" value="4">
                                                        <label for="star4"></label>
                                                        <input type="radio" id="star3" name="rating" value="3">
                                                        <label for="star3"></label>
                                                        <input type="radio" id="star2" name="rating" value="2">
                                                        <label for="star2"></label>
                                                        <input type="radio" id="star1" name="rating" value="1">
                                                        <label for="star1"></label>
                                                    </div>
                                                </div>
                                                <button class="btn add-to-cart3">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="related">
                <div class="col-md-12">
                    <h2 class="icon-css">Related Products</h2>
                    <div class="owl-carousel latest-products owl-theme wow fadeIn">

                        @foreach(findProductsForCategory($data->category_id, 10) as $product)
                        <div class="item">
                            <div class="product"> <a class="product-img" href="{{ route('customer.product.show', [$product->uuid]) }}"><img src="{{ asset($product->photos[0]->image) }}" alt=""></a>
                                <h5 class="product-type">{{ $product->category->name }}</h5>
                                <h3 class="product-name">{{ $product->name }}</h3>
                                <h3 class="product-price">&#x20A6; {{ number_format($product->price) }}</h3>
                                <div class="product-select">
                                    <button data-toggle="tooltip" data-placement="top" title="Quick view" class="add-to-compare round-icon-btn" data-fancybox="gallery" data-src="#product_details{{$product->id}}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                    <button data-toggle="tooltip" data-placement="top" title="Wishlist" class="add-to-wishlist round-icon-btn" onClick="window.location.href='#'"><i class="fa fa-heart-o" aria-hidden="true"></i></button>
                                    <button data-toggle="tooltip" data-placement="top" title="Add To Cart" onClick="window.location.href='{{ route("cart.add.item",[$product->uuid]) }}'" class="add-to-cart round-icon-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i></button>
                                </div>
                                @include('includes.modal', ['address'=> 'product_details'.$product->id, 'modal' => $product])
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script src="{{ asset('assets/vendor/detail-page/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/easyzoom.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/easyzoom-script.js') }}"></script>
<!--easyzoom-->
<!--bootstrap-->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!--bootstrap-->
<!--owl.carousel-->
<script src="{{ asset('assets/owlcarousel/owl.carousel.js') }}"></script>
<!--owl.carousel-->
<!--related-products-->
<script src="{{ asset('assets/vendor/detail-page/related-products.js') }}"></script>
<script src="{{ asset('assets/vendor/detail-page/index.js') }}"></script>
<!--related-products-->
<script src="{{ asset('assets/vendor/jquery/quality.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/product.js') }}"></script>





@endpush