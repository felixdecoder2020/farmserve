@section('contents')
@extends("layouts.home")
<div class="container page-h">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="login">
                <h2>Change Password</h2>
                <form method="POST" action="{{ route('password.store') }}">
                    @csrf
                    <!-- Password Reset Token -->
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">

                    <div class="form-group">
                        @if(session()->has('success'))
                        <p class="text-center text-success">{{session()->get('success')}}</p>
                        @endif
                        <p>Input email address.</p>
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Enter email">
                    </div>

                    <div class="form-group">Input password</p>
                        <input type="password" name="password" value="" class="form-control">
                    </div>
                    <div class="form-group">Confirm password</p>
                        <input type="password" name="password_confirmation" value="" class="form-control">
                    </div>

                    <div class="text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Reset Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection