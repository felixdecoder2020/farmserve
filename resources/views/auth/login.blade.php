@extends("layouts.home")

@section('contents')

<!-- Navigation -->
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8">
      <div class="login">
        <h2>Login</h2>
        <form action="{{ route('login') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="exampleInputEmail1"><strong>Username or email address *</strong></label>
            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><strong>Password *</strong></label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-6">
                <input name="remember" type="checkbox" value="">
                Save Password
              </div>
              <div class="col-6 text-right"><a href="{{ route('password.request') }}">Forget your Password</a></div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary">Sign in</button>
            <br>
            OR
            <br>
            <a href="{{route('register') }}" class="btn btn-primary">Register</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection