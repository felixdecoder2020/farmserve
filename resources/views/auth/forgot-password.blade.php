@section('contents')
@extends("layouts.home")
<div class="container page-h">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="login">
                <h2>Forgot Password</h2>
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group">
                        @if(session()->has('success'))
                        <p class="text-center text-success">{{session()->get('success')}}</p>
                        @endif
                        <p class="text-center">You can reset your password here.</p>
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Enter email">
                    </div>
                    <div class="text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Reset Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection