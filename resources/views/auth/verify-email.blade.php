@section('contents')
@extends("layouts.home")
<div class="container page-h">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="login">
                <h2>Resend Passowrd Link</h2>
                
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf
                    @if(session()->has('success'))
                    <p class="text-center text-success">{{session()->get('success')}}</p>
                    @endif

                    <p>Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn't receive the email, we will gladly send you another.</p>
                    <div class="text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Resend Verification</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection