@section('contents')
@extends("layouts.home")
<!-- Navigation -->
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb2">
            <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
            <li class="breadcrumb-item">Register</li>
        </ol>
    </nav>
    <form action="{{ route('register') }}" method="post">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="text-center mb-4 mt-4">
                    <h2>New Account</h2>
                </div>
            </div>

            @csrf

            <div class="col-lg-8 col-md-12 mb-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Email Address<small>Required</small></label>
                            <input name="email" type="text" class="form-control" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Password<small>Required</small></label>
                            <input name="password" type="password" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Confirm Password<small>Required</small></label>
                            <input name="password_confirmation" type="password" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">First Name<small>Required</small></label>
                            <input name="first_name" type="text" class="form-control" value="{{ old('first_name') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Last Name<small>Required</small></label>
                            <input name="last_name" type="text" class="form-control" value="{{ old('last_name') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Phone Number<small>Required</small></label>
                            <input name="phone" type="text" class="form-control" value="{{ old('phone') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Select State <small>Required</small></label>
                            <select name="state" class="form-control">
                                <option value="" selected="selected">- Select -</option>
                                <option value="Abuja FCT">Abuja FCT</option>
                                <option value="Abia">Abia</option>
                                <option value="Adamawa">Adamawa</option>
                                <option value="Akwa Ibom">Akwa Ibom</option>
                                <option value="Anambra">Anambra</option>
                                <option value="Bauchi">Bauchi</option>
                                <option value="Bayelsa">Bayelsa</option>
                                <option value="Benue">Benue</option>
                                <option value="Borno">Borno</option>
                                <option value="Cross River">Cross River</option>
                                <option value="Delta">Delta</option>
                                <option value="Ebonyi">Ebonyi</option>
                                <option value="Edo">Edo</option>
                                <option value="Ekiti">Ekiti</option>
                                <option value="Enugu">Enugu</option>
                                <option value="Gombe">Gombe</option>
                                <option value="Imo">Imo</option>
                                <option value="Jigawa">Jigawa</option>
                                <option value="Kaduna">Kaduna</option>
                                <option value="Kano">Kano</option>
                                <option value="Katsina">Katsina</option>
                                <option value="Kebbi">Kebbi</option>
                                <option value="Kogi">Kogi</option>
                                <option value="Kwara">Kwara</option>
                                <option value="Lagos">Lagos</option>
                                <option value="Nassarawa">Nassarawa</option>
                                <option value="Niger">Niger</option>
                                <option value="Ogun">Ogun</option>
                                <option value="Ondo">Ondo</option>
                                <option value="Osun">Osun</option>
                                <option value="Oyo">Oyo</option>
                                <option value="Plateau">Plateau</option>
                                <option value="Rivers">Rivers</option>
                                <option value="Sokoto">Sokoto</option>
                                <option value="Taraba">Taraba</option>
                                <option value="Yobe">Yobe</option>
                                <option value="Zamfara">Zamfara</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Address Line 2 </label>
                            <textarea name="address" type="text" class="form-control"></textarea>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-8 col-12">
                        <div class="form-group">
                            <input name="" type="submit" class="add-to-cart2 btn mt-31" value="Create Account">
                            <a href="{{route('login') }}" class="add-to-cart2 btn mt-31">login</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>

@endsection