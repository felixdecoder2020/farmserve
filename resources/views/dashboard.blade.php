@extends('layouts.home')
@section('contents')
<!-- Navigation -->
<div class="container mb-5">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb2">
      <li class="breadcrumb-item"><a href="{{route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
      <li class="breadcrumb-item">My Account</li>
    </ol>
  </nav>
  <div>
    <div class="account-dashboard">
      <div class="dashboard-upper-info">
        <div class="row align-items-center justify-content-center">
          <div class="col-lg-3 col-md-6">
            <div class="d-single-info">
              <div class="row">
                <div class="col-md-3"><img src="assets/images/logout.png" alt="" title="" class="img-fluid"></div>
                <div class="col-md-9">
                  <p class="user-name"><span class="text-danger">{{Auth::user()->email}}</span></p>
                  <p>( not {{Auth::user()->first_name}}? <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a> )</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="d-single-info">
              <div class="row">
                <div class="col-md-3"><img src="assets/images/support.png" alt="" title="" class="img-fluid"></div>
                <div class="col-md-9">
                  <p>Need Assistance? <br>
                    Customer service at</p>
                  <p><a href="mailto:{{ env('CONTACT_EMAIL') }}">{{ env('CONTACT_EMAIL') }}</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="d-single-info">
              <div class="row">
                <div class="col-md-3"><img src="assets/images/email.png" alt="" title="" class="img-fluid"></div>
                <div class="col-md-9">
                  <p>E-mail them at </p>
                  <p><a href="mailto:{{ env('CONTACT_EMAIL') }}">{{ env('CONTACT_EMAIL') }}</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="d-single-info border-0">
              <div class="row">
                <div class="col-md-12"><a href="{{route('cart.show') }}" class="view-cart"><i class="fa fa-cart-plus"></i>view cart </a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-lg-2">
          <!-- Nav tabs -->
          <ul role="tablist" class="nav flex-column dashboard-list" id="myTab">
            <li><a href="#dashboard" data-toggle="tab" class="active">Dashboard</a></li>
            <li> <a href="#orders" data-toggle="tab">Orders</a></li>
            <li><a href="#address" data-toggle="tab">Addresses</a></li>
            <li><a href="#account-details" data-toggle="tab">Account details</a></li>
            <li><a href="#update-password" data-toggle="tab">Account password</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a></li>
          </ul>
        </div>
        <div class="col-md-12 col-lg-10">
          <!-- Tab panes -->
          <div class="tab-content dashboard-content">
            <div class="tab-pane active" id="dashboard">
              <h3>Dashboard </h3>
              <div class="row">
                <div class="col-md-4 mb-3">
                  <div class="card">
                    <div class="card-body">
                      <div class="text-center"><a href="my-account.html"><img src="assets/images/page-img/lunch-box.png"></a></div>
                      <h2>Your Orders</h2>
                      <p>You currently have {{$order['pending']}} pending order, {{$order['successful']}} pending delivery, {{$order['processed']}} orders processed for delivery and {{$order['delivered']}} delivered order.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mb-3">
                  <div class="card">
                    <div class="card-body">
                      <div class="text-center"><a href="login.html"><img src="assets/images/page-img/login.png"></a></div>
                      <h2>Login & security</h2>
                      <p>Your account is secured. Worried?. Please <a href="#update-password" data-toggle="tab" style="color: rgb(229, 118, 58)"> Click Here</a> to change your password</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mb-3">
                  <div class="card">
                    <div class="card-body">
                      <div class="text-center"><a href="my-account.html"><img src="assets/images/page-img/notebook.png"></a></div>
                      <h2>Your Addresses</h2>
                      <p>Billing address: {{Auth::user()->address}}.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="orders">
              <h3>Orders</h3>
              <div class="table-responsive">
                <table class="table boder-b">
                  <thead>
                    <tr>
                      <th>Order</th>
                      <th>Date</th>
                      <th>payment</th>
                      <th>delivery</th>
                      <th>Total</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $key => $order)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $order->created_at->diffForHumans() }}</td>
                      <td>
                        @if($order->status == 1)
                        <button class="badge badge-success badge-pill" style="border:0; padding: 7px; width: 50%">successful</button>
                        @else
                        <button class="badge badge-danger badge-pill" style="border:0; padding: 7px; width: 50%">pending</button>
                        @endif
                      </td>
                      <td>
                        @if($order->delivery == 'pending')
                        <button class="badge badge-warning badge-pill" style="border:0; padding: 7px; width: 50%">pending</button>
                        @elseif($order->delivery == 'processed')
                        <button class="badge badge-primary badge-pill" style="border:0; padding: 7px; width: 50%">processed</button>
                        @elseif($order->delivery == 'delivered')
                        <button class="badge badge-success badge-pill" style="border:0; padding: 7px; width: 50%">delivered</button>
                        @else
                        <button class="badge badge-danger badge-pill" style="border:0; padding: 7px; width: 50%">waiting</button>
                        @endif

                      </td>
                      <td>&#x20A6;{{ number_format($order->amount,2) }} for 1 item </td>
                      <td><a href="{{ route('customer.order.show',[$order->uuid]) }}" class="view">view</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane fade" id="downloads">
              <h3>Downloads</h3>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Product</th>
                      <th>Downloads</th>
                      <th>Expires</th>
                      <th>Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Haven - Free Real Estate PSD Template</td>
                      <td>May 10, 2018</td>
                      <td>never</td>
                      <td><a href="#" class="view">Click Here To Download Your File</a></td>
                    </tr>
                    <tr>
                      <td>Nevara - ecommerce html template</td>
                      <td>Sep 11, 2018</td>
                      <td>never</td>
                      <td><a href="#" class="view">Click Here To Download Your File</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="address">
              <p>The following addresses will be used on the checkout page by default.</p>
              <h4 class="billing-address">Billing address</h4>
              <div class="row">
                <div class="col-md-4">
                  <div class="address-1">
                    <p class="biller-name"><strong>{{Auth::user()->first_name. ' ' . Auth::user()->last_name}}</strong></p>
                    <address>
                      <small>{{Auth::user()->address}}
                        <br>
                        {{Auth::user()->phone}}
                      </small>
                    </address>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="address-1">
                    <p class="biller-name"><strong>State</strong></p>
                    <address>
                      <small>{{Auth::user()->state}}</small>
                    </address>
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="account-details">
              <h3>Account details </h3>
              <div class="login m-0">
                <div class="login-form-container">
                  <div class="account-login-form">
                    <form action="{{ route('customer.profile.update') }}" method="post">
                      @csrf
                      <div class="account-input-box">
                        <div class="row">
                          <div class="col-md-6">
                            <label>First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{Auth::user()->first_name}}">
                          </div>
                          <div class="col-md-6">
                            <label>Last Name</label>
                            <input type="text" name="last_name" class="form-control" value="{{Auth::user()->last_name}}">
                          </div>
                          <div class="col-md-6">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control" value="{{Auth::user()->phone}}">
                          </div>
                          <div class="col-md-6">
                            <label>State</label>
                            <select name="state" class="form-control">
                              <option value="{{Auth::user()->state}}" selected="selected">{{Auth::user()->state}}</option>
                              <option value="Abuja FCT">Abuja FCT</option>
                              <option value="Abia">Abia</option>
                              <option value="Adamawa">Adamawa</option>
                              <option value="Akwa Ibom">Akwa Ibom</option>
                              <option value="Anambra">Anambra</option>
                              <option value="Bauchi">Bauchi</option>
                              <option value="Bayelsa">Bayelsa</option>
                              <option value="Benue">Benue</option>
                              <option value="Borno">Borno</option>
                              <option value="Cross River">Cross River</option>
                              <option value="Delta">Delta</option>
                              <option value="Ebonyi">Ebonyi</option>
                              <option value="Edo">Edo</option>
                              <option value="Ekiti">Ekiti</option>
                              <option value="Enugu">Enugu</option>
                              <option value="Gombe">Gombe</option>
                              <option value="Imo">Imo</option>
                              <option value="Jigawa">Jigawa</option>
                              <option value="Kaduna">Kaduna</option>
                              <option value="Kano">Kano</option>
                              <option value="Katsina">Katsina</option>
                              <option value="Kebbi">Kebbi</option>
                              <option value="Kogi">Kogi</option>
                              <option value="Kwara">Kwara</option>
                              <option value="Lagos">Lagos</option>
                              <option value="Nassarawa">Nassarawa</option>
                              <option value="Niger">Niger</option>
                              <option value="Ogun">Ogun</option>
                              <option value="Ondo">Ondo</option>
                              <option value="Osun">Osun</option>
                              <option value="Oyo">Oyo</option>
                              <option value="Plateau">Plateau</option>
                              <option value="Rivers">Rivers</option>
                              <option value="Sokoto">Sokoto</option>
                              <option value="Taraba">Taraba</option>
                              <option value="Yobe">Yobe</option>
                              <option value="Zamfara">Zamfara</option>
                            </select>
                          </div>
                          <div class="col-md-6">
                            <label>Address</label>
                            <textarea type="text" class="form-control" rows="5" name="address">{{Auth::user()->address}}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="button-box">
                        <button class="btn default-btn" type="submit">Update</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="update-password">
              <h3>Update Password </h3>
              <div class="login m-0">
                <div class="login-form-container">
                  <div class="account-login-form">
                    <form action="{{ route('customer.password.update') }}" method="post">
                      @csrf
                      <div class="account-input-box">
                        <div class="row">
                          <div class="col-md-6">
                            <label>Old password</label>
                            <input type="password" name="old_password" class="form-control">
                          </div>
                          <div class="col-md-6">
                            <label>New Password</label>
                            <input type="password" name="password" class="form-control">
                          </div>
                          <div class="col-md-6">
                            <label>Confirm new password</label>
                            <input type="password" name="password_confirmation" class="form-control">
                          </div>

                        </div>
                      </div>
                      <div class="button-box">
                        <button class="btn default-btn" type="submit">Update</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
      localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
      $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
  });
</script>
@endpush