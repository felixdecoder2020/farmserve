<!-- Logo Header -->
<!-- dark2 -->
<div class="logo-header" data-background-color="" style="background-color: rgb(57, 128, 83);">

    <a href="{{url('/')}}" class="logo">
        <img src="{{ asset('assets/images/logo.png') }}" width="150px" alt="navbar brand" class="navbar-brand">
    </a>
    <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">
            <i class="icon-menu"></i>
        </span>
    </button>
    <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
    <div class="nav-toggle">
        <button class="btn btn-toggle toggle-sidebar">
            <i class="icon-menu"></i>
        </button>
    </div>
</div>
<!-- End Logo Header -->
<!-- dark -->
<!-- Navbar Header -->
<nav class="navbar navbar-header navbar-expand-lg" data-background-color="" style="background-color: rgb(231, 233, 233); border-bottom: 1px solid #fff; ">

    <div class="container-fluid">

        <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">

            <li class="nav-item dropdown hidden-caret">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                    <div class="avatar-sm">
                        <span class="avatar-title rounded-circle text-white" style="background-color: rgb(57, 128, 83);">{{ substr(Auth::user()->first_name, 0, 1)  }}</span>
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-user animated fadeIn">
                    <div class="dropdown-user-scroll scrollbar-outer">
                        <li>
                            <div class="user-box">
                                <div class="avatar-lg">
                                    <span class="avatar-title rounded-circle bg-danger">{{ substr(Auth::user()->first_name, 0, 1) }}</span>
                                </div>
                                <div class="u-text">
                                    <h4>{{Auth::user()->first_name }} {{Auth::user()->last_name}}</h4>
                                    <p class="text-muted">{{Auth::user()->email}}</p><a href="{{ route('admin.user.profile') }}" class="btn btn-xs btn-success btn-sm">View Profile</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('admin.user.profile') }}">My Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('admin.user.profile') }}">Change Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                        </li>
                    </div>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<!-- End Navbar -->