<!DOCTYPE html>
<html lang="en">
@include('layouts.dashboard.head')
<!-- dark -->

<body data-background-color="" style="background-color: rgb(239, 239, 241)">
  <div class="wrapper">
    <div class="main-header">
      <!-- ======= Header and topnav ======= -->
      @include('layouts.dashboard.header')
      <!-- End Header and topnav -->
    </div>
    @include('layouts.dashboard.sidebar')
    <div class="main-panel">
      <div class="container">
        @yield('content')
      </div>
      <!-- ======= Footer ======= -->
      @include('layouts.dashboard.footer')
      <!-- End Footer -->
    </div>
  </div>
  @include('layouts.dashboard.scripts')
  @stack('script')
</body>

</html>