<!-- Sidebar -->

<!-- dark2 -->
<div class="sidebar sidebar-style-2" data-background-color="" style="background-color: rgb(57, 128, 83);">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <span class="avatar-title rounded-circle bg-white text-danger">{{ substr(Auth::user()->first_name, 0, 1)."".substr(Auth::user()->last_name, 0, 1) }}</span>
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{ Auth::user()->first_name. ' ' .Auth::user()->last_name}}
                            <span class="user-level text-warning">{{ Auth::user()->state }} State</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{ route('admin.user.profile') }}">
                                    <span class="link-collapse">My Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.user.profile') }}">
                                    <span class="link-collapse">Edit Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.user.password') }}">
                                    <span class="link-collapse">Change Password</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">

                <li class="nav-item {!! Request::is('admin/dashboard') ? 'active' : '' !!}">
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item {!! Request::is('admin/products') || Request::is('admin/product/create') || Request::is('admin/product/*') ? 'active' : '' !!}">
                    <a href="{{ route('admin.products') }}">
                        <i class="fas fa-th-large"></i>
                        <p>Manage Products</p>
                    </a>
                </li>

                <li class="nav-item {!! Request::is('admin/bank') ? 'active' : '' !!}">
                    <a href="{{ route('admin.bank.show') }}">
                        <i class="fas fa-piggy-bank"></i>
                        <p>Bank Details</p>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('admin/withdrawals') ? 'active' : '' !!}">
                    <a href="{{ route('admin.withdrawals') }}">
                        <i class="fas fa-money-bill"></i>
                        <p>Withdrawals</p>
                    </a>
                </li>


                <li class="nav-item {!! Request::is('admin/orders') || Request::is('admin/order/*')  ? 'active' : '' !!}">
                    <a href="{{ route('admin.orders') }}">
                        <i class="fas fa-arrows-alt"></i>
                        <p>Show Orders</p>
                    </a>
                </li>

                @if(Auth::user()->role === 'admin')
                <li class="nav-item {!! Request::is('admin/withdrawals/requests') ? 'active' : '' !!}">
                    <a href="{{ route('admin.withdrawals.request') }}">
                        <i class="fas fa-pound-sign"></i>
                        <p>Withdrawals Requests</p>
                    </a>
                </li>

                <li class="nav-item {!! Request::is('admin/categories') || Request::is('admin/categories/*') ? 'active' : '' !!}">
                    <a href="{{ route('category') }}">
                        <i class="fas fa-paperclip"></i>
                        <p>Categories</p>
                    </a>
                </li>

                <li class="nav-item {!! Request::is('admin/merchants') ||Request::is('admin/merchant/*') ? 'active' : '' !!}">
                    <a href="{{ route('admin.merchant') }}">
                        <i class="fas fa-book"></i>
                        <p>Merchants</p>
                    </a>
                </li>

                <li class="nav-item {!! Request::is('admin/users') || Request::is('admin/user/*') ? 'active' : '' !!}">
                    <a href="{{ route('admin.users') }}">
                        <i class="fas fa-users"></i>
                        <p>Show Users</p>
                    </a>
                </li>
                @endif

                <li class="nav-item {!! Request::is('admin/user/password') ? 'active' : '' !!}">
                    <a href="{{route('admin.user.password')}}">
                        <i class="fas fa-lock"></i>
                        <p>Change Password</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="route('logout')" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-xs btn-danger btn-sm">
                        <i class="fas fa-sign-out-alt text-white"></i>
                        <p class="text-white">Logout</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->