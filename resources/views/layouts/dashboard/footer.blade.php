<footer class="footer" style="background-color: rgba(239, 239, 241, 0.9)">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link text-danger" href="https://electionboot.com">
                        &copy; Copyright {{ now()->year }} {{ env('APP_NAME')}}
                    </a>
                </li>

            </ul>
        </nav>
        <div class="copyright ml-auto">

        </div>
    </div>
</footer>