<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/favicon.png') }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ env('APP_NAME') }} - {{ $title }}</title>
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('assets/css/home-1.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/revolution/vendor/revslider/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/revolution/responsiveslides.css') }}">
    @stack('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/toastr.css') }}">
    <!-- main css -->
</head>

<body>
    @yield('subscribe')
    <!-- Navigation -->
    <div class="top-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12 top-div top1">
                    <ul>
                        <li><a href="mailto:info.organicstore@gmail.com"><i class="fa fa-envelope"></i> &nbsp; {{ env('CONTACT_EMAIL') }}</a></li>
                        <li>|</li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i> {{ env('CONTACT_PHONE') }}</li>
                    </ul>
                </div>
                <div class="col-lg-7 col-md-6 col-md-12 position-relative">
                    <div class="right-div">
                        <ul>
                            <li>
                                <ul class="social-network">
                                    <li><a href="#" class="icoRss" title=""><i class="fa fa-rss"></i></a></li>
                                    <li><a href="#" class="icoFacebook" title=""><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="icoTwitter" title=""><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="icoGoogle" title=""><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#" class="icoLinkedin" title=""><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </li>
                            <li>
                                <ul class="top-ul">
                                    <li>
                                        <div class="dropdown"> <a class="dropdown-toggle" href="#" data-toggle="dropdown"><img src="{{ asset('assets/images/flag.png') }}" width="22px" alt="" title=""> English <i class="fa fa-angle-down"></i></a>
                                            <div class="dropdown-menu flag-css dropdown-menu-right"> <a href="#">English</a> <a href="#"><span class="flag-icon flag-icon-fr"> </span>Yoruba</a> <a href="#"><span class="flag-icon flag-icon-it"> </span>Tiv</a> <a href="#"><span class="flag-icon flag-icon-ru"> </span>Hausa</a> </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="dropdown"> <a class="dropdown-toggle currency" href="#" data-toggle="dropdown"> <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu drop1 dropdown-menu-right">
                                                <li class="dropdown-item"><a href="#"> NGN Naira</a></li>
                                                <li class="dropdown-item"><a href="#"> US Dollar</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div class="rate-price rate-price2">
                                    <ul>
                                        <li class="dropdown"> <a class="dropdown-toggle" href="#" data-toggle="dropdown"> <i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right animate slideIn">

                                                @if(Auth::check())
                                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                                @else
                                                <a class="dropdown-item" href="{{route('login') }}">Login</a>
                                                @endif

                                                <a class="dropdown-item" href="{{ route('dashboard') }}">My Account</a>

                                                @if(!Auth::check())
                                                <a class="dropdown-item" href="{{ route('register') }}">Register</a>
                                                <a class="dropdown-item" href="{{ route('password.request') }}">Forgot Password</a>
                                                @endif
                                            </div>
                                        </li>

                                        <!-- Wish list -->
                                        <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i><span class="circle-2">0</span></a></li>

                                        <li class="dropdown"> <a class="dropdown-toggle link" href="#" data-toggle="dropdown"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span class="circle-2">

                                                    @if(Auth::check())

                                                    {{ Auth::user()->carts->count()}}
                                                    @else
                                                    0
                                                    @endif

                                                </span></a>
                                            <div class="dropdown-menu dropdown-menu2 dropdown-menu-right  animate slideIn">
                                                <div class="container">
                                                    <div class="row">
                                                        @if(Auth::check())
                                                        @php
                                                        $sum = 0;
                                                        @endphp

                                                        @foreach(Auth::user()->carts as $data)
                                                        @php
                                                        $sum += $data->product->price * $data->quantity;
                                                        @endphp
                                                        <div class="col-3"><img src="{{ asset($data->product->photos[0]->image) ?? asset('assets/images/fruits/img-1.jpg') }}" alt="" title="" class="img-fluid"></div>
                                                        <div class="col-9">
                                                            <p>{{ $data->quantity }} x {{$data->product->name}} <span class="price">&#x20A6; {{$data->product->price}}</span></p>
                                                            <a href="{{ route('cart.delete.item',[$data->uuid]) }}" style="color:black" title="Delete item from cart" class="close">x</a>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <hr>
                                                        </div>
                                                        @endforeach
                                                        <div class="col-3">
                                                            <p class="font-15"><strong>Total</strong></p>
                                                        </div>
                                                        <div class="col-9 text-right"> <span class="font-15"><strong> &#x20A6; {{ number_format($sum, 2) }}</strong></span> </div>
                                                        <div class="col-md-12">
                                                            <hr>
                                                        </div>
                                                        @endif
                                                        <div class="col-md-12 text-center">
                                                            <a href="{{ route('cart.show') }}" class="btn check-out w-100">Check out</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>


                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-white">
        <div class="container"> <a class="navbar-brand" href="{{ route('index') }}"> <img src="{{ asset('assets/images/logo.png') }}" alt="" title="" class="img-fluid"> </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> <span class="navbar-toggler-icon"></span> <span class="navbar-toggler-icon"></span> </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('index') }}"> Home </a></li>

                    <li class="nav-item dropdown megamenu-li"> <a class="nav-link nav-link-dropdown dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> SHOP </a>
                        <div class="dropdown-menu megamenu animate slideIn">

                            <div class="row">
                                <div class="col-sm-6 col-lg-4">
                                    <h5>Latest Vendors</h5>
                                    @foreach(vendorList('latest') as $data)
                                    <a class="dropdown-item" href="#">{{ $data->last_name}}</a>
                                    @endforeach
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <h5>Most Popular</h5>
                                    @foreach(vendorList('popular') as $data)
                                    <a class="dropdown-item" href="#">{{ $data->last_name}}</a>
                                    @endforeach
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <h5>Random</h5>
                                    @foreach(vendorList('random') as $data)
                                    <a class="dropdown-item" href="#">{{ $data->last_name}}</a>
                                    @endforeach
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-6"><img src="assets/images/page-img/menu-img.jpg" alt="" title="" class="img-fluid"></div>
                                <div class="col-md-6"><img src="assets/images/page-img/menu2-img.jpg" alt="" title="" class="img-fluid"></div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown megamenu-li"> <a class="nav-link nav-link-dropdown dropdown-toggle" href="#" id="Dropdown2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Category </a>
                        <div class="dropdown-menu megamenu animate slideIn" aria-labelledby="navbarDropdownBlog">
                            <div class="row">

                                @foreach(randomCategory() as $data)
                                <div class="col-sm-6 col-lg-3">
                                    <h5>{{$data->name}}</h5>

                                    @foreach(findProductsForCategory($data->id) as $data)
                                    <a class="dropdown-item" href="{{ route('customer.product.show', [$data->uuid]) }}">{{ $data->name}}</a>
                                    @endforeach
                                </div>
                                @endforeach
                                <div class="col-sm-6 col-lg-3">
                                    <p><img src="{{ asset( (randomCategory()[0]->background ?? null) ) }}" alt="" title="" class="img-fluid"></p>
                                </div>
                                <a href="{{route('filter.category',['all']) }}" class="btn btn-primary">Show all Categories</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('index') }}"> About </a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('index') }}"> Contact </a></li>
                </ul>
                <div class="rate-price nav-1">
                    <ul>
                        <li class="dropdown"> <a class="dropdown-toggle" href="#" data-toggle="dropdown"> <i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animate slideIn">

                                @if(Auth::check())
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                @else
                                <a class="dropdown-item" href="{{route('login') }}">Login</a>
                                @endif

                                <a class="dropdown-item" href="{{ route('dashboard') }}">My Account</a>

                                @if(!Auth::check())
                                <a class="dropdown-item" href="{{ route('register') }}">Register</a>
                                <a class="dropdown-item" href="{{ route('password.request') }}">Forgot Password</a>
                                @endif
                            </div>
                        </li>
                        <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i><span class="circle-2">0</span></a></li>
                        <li class="dropdown"> <a class="dropdown-toggle link" href="#" data-toggle="dropdown"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span class="circle-2">
                                    @if(Auth::check())

                                    {{Auth::user()->carts->count()}}
                                    @else
                                    0
                                    @endif
                                </span></a>
                            <div class="dropdown-menu dropdown-menu2 dropdown-menu-right animate slideIn">
                                <div class="container">
                                    <div class="row">

                                        @if(Auth::check())
                                        @php
                                        $sum = 0;
                                        @endphp

                                        @foreach(Auth::user()->carts as $data)
                                        @php
                                        $sum += $data->product->price * $data->quantity;
                                        @endphp
                                        <div class="col-3"><img src="{{ asset($data->product->photos[0]->image) ?? asset('assets/images/fruits/img-1.jpg') }}" alt="" title="" class="img-fluid"></div>
                                        <div class="col-9">
                                            <p>{{ $data->quantity }} x {{$data->product->name}} <span class="price">&#x20A6; {{$data->product->price}}</span></p>
                                            <a href="{{ route('cart.delete.item',[$data->uuid]) }}" title="Delete item from cart" class="close">x</a>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        @endforeach

                                        <div class="col-md-3">
                                            <p class="font-15"><strong>Total</strong></p>
                                        </div>
                                        <div class="col-md-9 text-right"> <span class="font-15"><strong>&#x20A6; {{ number_format($sum, 2) }}</strong></span> </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        @endif
                                        <div class="col-md-12 text-center">
                                            <a href="{{ route('cart.show') }}" class="btn check-out w-100">Check out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </nav>

    @yield('contents')

    <div id="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h4>Join Our Newsletter Now</h4>
                    <p class="m-0">Get E-mail updates about our latest shop and special offers.</p>
                </div>
                <div class="col-md-5">
                    <form action="http://www.creativethemes.co.in/" method="post" id="subsForm" onSubmit="return ajaxmailsubscribe();">
                        <div class="input-group">
                            <input type="email" name="subsemail" id="subsemail" class="form-control newsletter" placeholder="Enter your mail">
                            <span class="input-group-btn">
                                <button class="btn btn-theme" type="button" onClick="return ajaxmailsubscribe();">Subscribe</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!--bootstrap-->
    <!-- Footer -->
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 address wow fadeInLeft">
                <div class="footer-logo"><img src="assets/images/logo.png" alt="" title="" class="img-fluid"></div>
                <p>Address: 123-45 Road 11378 Manchester</p>
                <p>Phone: +12 3456 78901</p>
                <p>Email: <a href="mailto:info.organicstore@gmail.com">info.organicstore@gmail.com</a></p>
                <ul class="social-2">
                    <li><a href="#" title="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="instagram +"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" title="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="Linkedin"><i class="fa fa-pinterest"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 footer-link  wow fadeInLeft">
                <h3>Information</h3>
                <ul>
                    <li><a href="about-us.html">About Us</a></li>
                    <li><a href="faq.html">FAQ</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <li><a href="shop.html">Shop</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 footer-link  wow fadeInLeft">
                <h3>My Account</h3>
                <ul>
                    <li><a href="{{ route('dashboard') }}">My Account</a></li>

                    @if(Auth::check())
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                    @else
                    <li><a href="{{route('login') }}">Login</a></li>
                    @endif
                    <li><a href="wishlist.html">Wishlist</a></li>
                    @if(!Auth::check())
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @endif
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 footer-link  wow fadeInLeft">
                <h3>Quick Link</h3>
                <ul>
                    <li><a href="cart.html">Cart</a></li>
                    <li><a href="wishlist.html">Wishlist</a></li>
                    <li><a href="comingsoon.html">Coming Soon</a></li>
                    <li><a href="404.html">404</a></li>
                </ul>
            </div>
        </div>
    </div>
    <footer class="py-4 bg-dark">
        <div class="container copy-right">
            <div class="row">
                <div class="col-md-6 text-white"> Copyright © {{ date('Y') }} <a href="#">{{env('APP_NAME')}} Store </a>- All Rights Reserved. </div>
                <div class="col-md-6 payment">
                    <div class="pull-right"> <a href="#"><img src="assets/images/skrill.png" alt="" title=""></a> <a href="#"><img src="assets/images/ob.png" alt="" title=""></a> <a href="#"><img src="assets/images/paypal.png" alt="" title=""></a> <a href="#"><img src="assets/images/am.png" alt="" title=""></a> <a href="#"><img src="assets/images/mr.png" alt="" title=""></a> <a href="#"><img src="assets/images/visa.png" alt="" title=""></a> </div>
                </div>
            </div>
        </div>
    </footer>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    <script src="{{ asset('assets/vendor/home-banner/jquery.min.js') }}"></script>


    <script src="{{ asset('assets/js/ajax.js') }}"></script>
    <script src="{{ asset('assets/js/formValidation.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--bootstrap-->
    <!--owlcarousel-->
    <script src="{{ asset('assets/owlcarousel/owl.carousel.js') }}"></script>
    <!--owlcarousel-->
    <!--script-->
    <script src="{{ asset('assets/vendor/jquery/home-1.js') }}"></script>
    <!--script-->
    <!-- animation-->
    <script src="{{ asset('assets/vendor/wow/wow.js') }}"></script>
    <script src="{{ asset('assets/vendor/wow/page.js') }}"></script>
    <!-- animation-->
    <!--select-dropdown-->
    <script src="{{ asset('assets/vendor/jquery/custom-select.js') }}"></script>
    <!--select-dropdown-->
    <!--fancybox files -->
    <link rel="stylesheet" href="{{ asset('assets/css/product-hover.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fancy-box/fancybox.min.css') }}" />
    <script src="{{ asset('assets/vendor/fancy-box/jquery.fancybox.min.js') }}"></script>
    <!--fancybox files -->
    <!--banner js-->
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/jquery.themepunch.tools.min.77') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/jquery.themepunch.revolution.m') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/extens/revolution.extension.ac') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/extens/revolution.extension.la') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/extens/revolution.extension.na') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/extens/revolution.extension.pa') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/vendor/revslider/js/extens/revolution.extension.sl') }}"></script>
    <script src="{{ asset('assets/js/banner.js') }}"></script>
    <script src="{{ asset('assets/js/toastr.js') }}"></script>
    <!--banner js-->
    <!--scrolltop-->
    <script src="{{ asset('assets/vendor/jquery/scrolltopcontrol.js') }}"></script>
    <script src="{{ asset('assets/vendor/revolution/responsiveslides.min.js') }}"></script>

    @stack('scripts')

    @if(session()-> has('error'))
    <script type="text/javascript">
        toastr.error("{{session()->get('error') }}");
    </script>
    @endif

    @if(session()-> has('success'))
    <script type="text/javascript">
        toastr.success("{{session()->get('success') }}");
    </script>
    @endif

    @if($errors->any())
    @foreach ($errors->all() as $key => $error)
    <script type="text/javascript">
        toastr.error("{{$error}}");
    </script>
    @endforeach
    @endif
</body>

</html>